Getting-Started with VulkanSDK
==============================

GNU/Linux Dependencies
----------------------

- `libxcb1-dev`
- `xorg-dev`

Setup
-----

- Install [CMake](https://cmake.org/)
- Install [Vulkan SDK](https://vulkan.lunarg.com/)
- Have a working C++ compiler with C++14 support
- Update GPU drivers
- Clone this repository
- Run `python download.py` or download dependencies manually and link sdk folder
  - **Only do the following if you don't want to run `download.py`**
  - mkdir `external`
  - download and extract [glfw-3.2.1](http://www.glfw.org/) into `external`
  - download and extract [glm-0.9.8.x](https://glm.g-truc.net/0.9.8/index.html) into `external`
  - download and extract [lua-5.3.4](http://www.lua.org/download.html) into `external`
  - donwload [tinyobjloader.h](https://github.com/syoyo/tinyobjloader/blob/master/tiny_obj_loader.h) into `external`
  - donwload [stb_image.h](https://github.com/nothings/stb/blob/master/stb_image.h) into `external`
  - download [json.hpp](https://raw.githubusercontent.com/nlohmann/json/develop/single_include/nlohmann/json.hpp) into `external`
  - Windows:
      - `mklink /J external\sdk <PATH_TO_VULKAN_SDK>\<VERSION_NUMBER>`
      - copy `vulkan-1.dll` to `vulkan.dll` (MinGW)
      - copy `vulkan-1.lib` to `vulkan.lib` (MSVC)
  - GNU/Linux:
      - `ln -s <PATH_TO_VULKAN_SDK>/<VERSION_NUMBER>/x86_64 external/sdk`
- Download assets from [here](https://www.dropbox.com/sh/fvxag57iwbo96u8/AACpCtkGCSRQmZgMXkd8MuOsa?dl=0)
  - Choose `Download -> Direct Download`
  - Extract `vulkan-assets.zip` into `vulkan-getting-started`

Build
-----

- `mkdir debug` and `cd build`
- `cmake ..`
- Build with your toolchain

CLI
---

*Note: on GNU/Linux the commands must be prefixed with `./`*

- `base` runs the application with the default scene (`scenes/cube.json`)
- `base <scene-name>` runs the application with the scene `scenes/<scene-name>.json`
- `base <scene-name> y` runs the application with the given scene and enables validation layers


Hotkeys
-------

- `W A S D` move the camera forward, left, back and right
- `LSHIFT LCTRL` move the camera up and down
- `P O` switch to **p**erspective or **o**rthographic projection
- `NUM+ NUM-` increase/decrease perspective FOV or orthografic size (depends on current projection mode)