#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
} global;

layout(binding = 2) uniform LocalUniform
{
    mat4 model;
    mat4 normal;
    vec3 color;
} local;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;
layout(location = 3) in vec2 inTexCoord;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outPosition;
layout(location = 2) out vec3 outNormal;
layout(location = 3) out vec2 outTexCoord;
layout(location = 4) out vec3 outTintColor;

layout(push_constant) uniform Push
{
    float totalTime;
} push;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    gl_Position = local.model * vec4(inPosition, 1.0);
    outPosition = gl_Position.xyz;
    gl_Position = global.projection * global.view * gl_Position;
    outColor = vec4(inColor, 1.0);
    outNormal = (local.normal * vec4(inNormal, 0)).xyz;
    outTexCoord = inTexCoord;
    outTintColor = local.color;
}