#version 450
#extension GL_ARB_separate_shader_objects : enable

#define MAX_NUMBER_OF_LIGHTS 10

layout(constant_id = 0) const int NUMBER_OF_LIGHTS = 0;

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
    vec3 camera;
} global;

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inPosition;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;
layout(location = 4) in vec3 inTintColor;

const vec3 lightPosition = vec3(3, -3, -3);
const vec4 ambientColor = vec4(0.1, 0.1, 0.1, 1);
const vec4 specularColor = vec4(1, 1, 1, 0);
const float roughness = 60;

layout(push_constant) uniform Light
{
    layout(offset = 16) vec3 positions[MAX_NUMBER_OF_LIGHTS];
} light;

const float TWO_PI = 3.1415926535897932384626433832795 * 2;

void main()
{
    outColor = ambientColor * vec4(inTintColor, 1) * vec4(inTintColor, 1);
    outColor += texture(texSampler, inTexCoord) * 0.01 * vec4(inTintColor, 1);

    vec3 normal = normalize(inNormal);

    for(int i = 0; i < NUMBER_OF_LIGHTS; ++i)
    {
        vec3 lightDirection = normalize(light.positions[i] - inPosition);

        float factor = dot(lightDirection, normal);


        if(factor > 0)
        {
            outColor += texture(texSampler, inTexCoord) * factor * vec4(inTintColor, 1);

            vec3 cameraDirection = normalize(inPosition - global.camera);
            vec3 reflected = reflect(-lightDirection, normal);

            factor = max(-dot(reflected, cameraDirection), 0);

            factor = pow(factor, roughness) * (roughness + 2) / TWO_PI;
            outColor += specularColor * factor;
        }
    }
}
