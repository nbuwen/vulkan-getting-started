#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
} global;

layout(binding = 1) uniform sampler2D texSampler;

layout(binding = 2) uniform LocalUniform
{
    mat4 model;
    mat4 normal;
    vec3 color;
} local;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;
layout(location = 3) in vec2 inTexCoord;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outPosition;
layout(location = 2) out vec3 outNormal;
layout(location = 3) out vec2 outTexCoord;
layout(location = 4) out vec3 outTintColor;

layout(push_constant) uniform Push
{
    float totalTime;
} push;

out gl_PerVertex
{
    vec4 gl_Position;
};

const ivec2 SIZE = {16, 16};

void main()
{
    gl_Position = vec4(inPosition, 1.0);

    const int x = gl_InstanceIndex % SIZE.x;
    const int y = gl_InstanceIndex / SIZE.x;

    gl_Position += vec4(x, 0, y, 0);
    gl_Position.x /= SIZE.x;
    gl_Position.z /= SIZE.y;
    gl_Position = gl_Position;

    outTexCoord = (inTexCoord + vec2(x, -y - 1)) / SIZE;

    gl_Position.y = -texture(texSampler, outTexCoord).a;
}