#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
} global;

layout(binding = 2) uniform LocalUniform
{
    mat4 model;
    mat4 normal;
    vec3 color;
} local;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;
layout(location = 3) in vec2 inTexCoord;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outPosition;
layout(location = 2) out vec3 outNormal;
layout(location = 3) out vec2 outTexCoord;
layout(location = 4) out vec3 outTintColor;

layout(push_constant) uniform Push
{
    float totalTime;
} push;

out gl_PerVertex
{
    vec4 gl_Position;
};

float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec2 PLANE = vec2(1500, 1500);

void main()
{
    gl_Position = vec4(inPosition, 1.0);
    gl_Position = gl_Position;



    gl_Position = local.model * gl_Position;

    vec4 instanceOffset = vec4(0, 0, 0, 0);
    vec2 seed = vec2(gl_InstanceIndex, 0);
    instanceOffset.x = rand(seed) * PLANE.x - PLANE.x/2.0;
    seed.y = 1;
    instanceOffset.z = rand(seed) * PLANE.y - PLANE.y/2.0;
    instanceOffset = global.view * instanceOffset;

    mat4 view = global.view;

    view[0][0] = 1;
    view[0][1] = 0;
    view[0][2] = 0;

    view[2][0] = 0;
    view[2][1] = 0;
    view[2][2] = 1;

    gl_Position = view * gl_Position;
    gl_Position += instanceOffset;



    gl_Position = global.projection * gl_Position;

    outTexCoord = inTexCoord;
}