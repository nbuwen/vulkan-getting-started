#version 450
#extension GL_ARB_separate_shader_objects : enable

#define MAX_NUMBER_OF_LIGHTS 10

layout(constant_id = 0) const int NUMBER_OF_LIGHTS = 0;

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
    vec3 camera;
} global;

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inPosition;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;
layout(location = 4) in vec3 inTintColor;

const vec4 ambientColor = vec4(0.1, 0.1, 0.1, 1);
const vec4 specularColor = vec4(1, 1, 1, 0);

layout(push_constant) uniform Light
{
    layout(offset = 16) vec3 positions[MAX_NUMBER_OF_LIGHTS];
} light;

float scale(float x, float s, float o)
{
    return 0.5 * tanh(s*2*3.14159*x - s*3.14159 + o) + 0.5;
}

void main()
{
    vec3 normal = normalize(inNormal);
    vec2 nightTexCoord = inTexCoord;
    nightTexCoord.y += 0.5;

    for(uint i = 0; i < NUMBER_OF_LIGHTS; ++i)
    {
        vec3 lightDirection = normalize(light.positions[i] - inPosition);

        float factor = scale(max(0, dot(lightDirection, normal)), 2, 4);

        outColor += texture(texSampler, inTexCoord) * factor;
        outColor += texture(texSampler, nightTexCoord) * (1 - factor);

    }
}
