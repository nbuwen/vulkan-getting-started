#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
} global;

layout(binding = 1) uniform sampler2D texSampler;

layout(binding = 2) uniform LocalUniform
{
    mat4 model;
    mat4 normal;
    vec3 color;
} local;

layout(triangles, equal_spacing, cw) in;

layout(location = 0) in vec3 inNormal[];
layout(location = 1) in vec2 inTexCoord[];

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outPosition;
layout(location = 2) out vec3 outNormal;
layout(location = 3) out vec2 outTexCoord;
layout(location = 4) out vec3 outTintColor;

const vec2 size = {2.0, 0.0};

void main()
{
    gl_Position = vec4(0, 0, 0, 1);
    outTexCoord = vec2(0, 0);

    gl_Position += gl_in[0].gl_Position * gl_TessCoord[0];
    outTexCoord += inTexCoord[0] * gl_TessCoord[0];
    gl_Position += gl_in[1].gl_Position * gl_TessCoord[1];
    outTexCoord += inTexCoord[1] * gl_TessCoord[1];
    gl_Position += gl_in[2].gl_Position * gl_TessCoord[2];
    outTexCoord += inTexCoord[2] * gl_TessCoord[2];

    gl_Position.y = -texture(texSampler, outTexCoord).a;
    gl_Position = global.projection * global.view * local.model * gl_Position;

}
