#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(vertices = 3) out;

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
    vec3 camera;
} global;

layout(binding = 2) uniform LocalUniform
{
    mat4 model;
    mat4 normal;
    vec3 color;
} local;

layout(location = 0) in vec3 ignored[];
layout(location = 1) in vec3 ignored2[];
layout(location = 2) in vec3 inNormal[];
layout(location = 3) in vec2 inTexCoord[];
layout(location = 4) in vec3 ignored3[];

layout(location = 0) out vec3 outNormal[3];
layout(location = 1) out vec2 outTexCoord[3];


float level(vec4 eye, vec4 point)
{
    float d = distance(eye, point);
    if(d < 10) return 64;
    if(d < 50) return 32;
    if(d < 100) return 16;
    if(d < 200) return 8;
    if(d < 400) return 4;
    if(d < 800) return 2;
    return 1;
}

void main()
{
    if(gl_InvocationID == 0)
    {
        // FROM http://in2gpu.com/2014/06/27/stitching-and-lod-using-tessellation-shaders-for-terrain-rendering/
        vec4 eye = vec4(global.camera, 1);

        mat4 mat = local.model;
        vec4 v0 = mat * gl_in[0].gl_Position / 2;
        vec4 v1 = mat * gl_in[1].gl_Position / 2;
        vec4 v2 = mat * gl_in[2].gl_Position / 2;

        vec3 d1 = v1.xyz + (v2.xyz - v1.xyz) / 2;
        vec3 d2 = v0.xyz + (v2.xyz - v0.xyz) / 2;
        vec3 d3 = v0.xyz + (v1.xyz - v0.xyz) / 2;

        float e0 = level(vec4(d1, 1), eye);
        float e1 = level(vec4(d2, 1), eye);
        float e2 = level(vec4(d3, 1), eye);
        float mine = min(e0, min(e1, e2));
        float maxe = max(e0, max(e1, e2));

        gl_TessLevelInner[0] = floor((mine + maxe) / 2);
        gl_TessLevelOuter[0] = e0;
        gl_TessLevelOuter[1] = e1;
        gl_TessLevelOuter[2] = e2;
    }

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
    outTexCoord[gl_InvocationID] = inTexCoord[gl_InvocationID];
}
