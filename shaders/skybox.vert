#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
} global;

layout(binding = 2) uniform LocalUniform
{
    mat4 model;
    mat4 normal;
    vec3 color;
} local;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;
layout(location = 3) in vec2 inTexCoord;

layout(location = 0) out vec3 outDirection;

layout(push_constant) uniform Push
{
    float totalTime;
} push;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    mat4 model = mat4(1);
    model *= 1000;
    model[3][3] = 1;

    gl_Position = model * vec4(inPosition, 1.0);
    gl_Position[3] = 0;
    gl_Position = global.view * gl_Position;
    gl_Position[3] = 1;
    gl_Position = global.projection * gl_Position;
    outDirection = inPosition;
}