from __future__ import print_function

from os.path import splitext, join, exists, getmtime
from os import mkdir, listdir, system
from errno import EEXIST
from platform import system as system_name


COMPILED_FOLDER = 'compiled'
COMPILED_EXT = '.spv'
if system_name() == 'Windows':
    COMPILE_COMMAND = '..\\external\\sdk\\bin\\glslangValidator.exe -V {} -o {}'
else:
    COMPILE_COMMAND = '../external/sdk/bin/glslangValidator -V {} -o {}'


def compiled_path(file_name):
    return join(COMPILED_FOLDER, file_name) + COMPILED_EXT


def create_folder(folder):
    try:
        mkdir(folder)
    except OSError as e:
        if e.errno != EEXIST:
            raise


def is_shader(file_name):
    _, ext = splitext(file_name)
    return ext in ('.vert', '.frag', '.tesc', '.tese')


def needs_recompile(shader):
    compiled = compiled_path(shader)
    if not exists(compiled):
        return True
    return getmtime(shader) > getmtime(compiled)


def recompile(shader):
    system(COMPILE_COMMAND.format(shader, compiled_path(shader)))


def main():
    create_folder(COMPILED_FOLDER)

    shaders = filter(is_shader, listdir('.'))
    shaders = list(filter(needs_recompile, shaders))

    print("compiling", len(shaders), "new or modified shaders")

    for shader in shaders:
        recompile(shader)


if __name__ == '__main__':
    main()
