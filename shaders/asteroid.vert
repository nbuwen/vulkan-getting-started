#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
} global;

layout(binding = 2) uniform LocalUniform
{
    mat4 model;
    mat4 normal;
    vec3 color;
} local;

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inColor;
layout(location = 3) in vec2 inTexCoord;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outPosition;
layout(location = 2) out vec3 outNormal;
layout(location = 3) out vec2 outTexCoord;
layout(location = 4) out vec3 outTintColor;

layout(push_constant) uniform Push
{
    float time;
} push;

out gl_PerVertex
{
    vec4 gl_Position;
};

mat4 rotate(float angle, vec3 axis)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;

    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float rand(float x, float y)
{
    return rand(vec2(x, y));
}

void main()
{

    gl_Position = vec4(inPosition, 1.0);
    vec3 axis = vec3(rand(gl_InstanceIndex, 3.14), rand(gl_InstanceIndex, 1.41), rand(gl_InstanceIndex, 2.71));
    axis = axis * 2 - 1;
    mat4 rotation = rotate(push.time, axis);
    gl_Position = rotation * gl_Position;
    gl_Position = local.model * gl_Position;

    int x = gl_InstanceIndex % 10;
    int y = gl_InstanceIndex / 10;

    gl_Position.x += x / 30.0;
    gl_Position.z += y / 30.0;
    float d = sqrt(x*x + y*y) / 300;
    gl_Position.y += d * (rand(vec2(gl_InstanceIndex, 42)) * 2 - 1);

    float timeAngle = push.time * rand(vec2(gl_InstanceIndex, 1337)) / (d * 10);
    float phase = 6.2831 * rand(vec2(gl_InstanceIndex, 123));
    mat4 model = rotate(timeAngle + phase, vec3(0, 1, 0));

    gl_Position = global.projection * global.view * model * gl_Position;

    outColor = vec4(inColor, 1.0);
    outNormal = mat3(local.normal) * mat3(rotation) * inNormal;
    outTexCoord = inTexCoord;
    outTintColor = local.color;
}