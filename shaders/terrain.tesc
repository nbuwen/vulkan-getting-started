#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout(vertices = 3) out;

layout(binding = 0) uniform GlobalUniform
{
    mat4 view;
    mat4 projection;
    vec3 camera;
} global;

layout(binding = 2) uniform LocalUniform
{
    mat4 model;
    mat4 normal;
    vec3 color;
} local;

layout(location = 0) in vec3 ignored[];
layout(location = 1) in vec3 ignored2[];
layout(location = 2) in vec3 inNormal[];
layout(location = 3) in vec2 inTexCoord[];
layout(location = 4) in vec3 ignored3[];

layout(location = 0) out vec3 outNormal[3];
layout(location = 1) out vec2 outTexCoord[3];


void main()
{
    if(gl_InvocationID == 0)
    {
        gl_TessLevelInner[0] = 64;
        gl_TessLevelOuter[0] = 64;
        gl_TessLevelOuter[1] = 64;
        gl_TessLevelOuter[2] = 64;
    }

    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
    outTexCoord[gl_InvocationID] = inTexCoord[gl_InvocationID];
}
