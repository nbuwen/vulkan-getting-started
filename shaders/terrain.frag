#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

#define MAX_NUMBER_OF_LIGHTS 10

layout(constant_id = 0) const int NUMBER_OF_LIGHTS = 0;

layout(binding = 1) uniform sampler2D texSampler;

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inPosition;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inTexCoord;
layout(location = 4) in vec3 inTintColor;

layout(push_constant) uniform Light
{
    layout(offset = 16) vec3 positions[MAX_NUMBER_OF_LIGHTS];
} light;

const ivec3 off = {-1, 0, 1};
const float scale = 0.01;

void main()
{
    outColor = vec4(texture(texSampler, inTexCoord).rgb, 1);
    //outColor = vec4(1, 0.5, 0.5, 1);
}
