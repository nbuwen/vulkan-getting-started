from __future__ import print_function

from errno import EEXIST
from zipfile import ZipFile
from tarfile import open as tarfile
from os import mkdir, sep, listdir, system as system_command, remove
from os.path import exists, abspath, join
from re import match
from sys import stdout, argv
from platform import system as get_system


class Tarfile:
    def __init__(self, name, mode):
        self._tarfile = tarfile(name, mode)

    def namelist(self):
        return self._tarfile.getnames()

    def extractall(self, *args):
        self._tarfile.extractall(*args)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._tarfile.close()


try:
    # python 2
    import urllib2
    MODE = 'wx'
    input_ = globals()['__builtins__'].raw_input
    
    def print_(*args, **kwargs):
        flush = kwargs.pop('flush', False)
        print(*args, **kwargs)
        if flush:
            stdout.flush()
    
except ImportError:
    # python 3
    import urllib.request as urllib2
    MODE = 'bx'
    input_ = globals()['__builtins__'].input
    print_ = globals()['__builtins__'].print

PREFIX = 'external'
SDK_FOLDER = 'sdk'
VERSION_REGEX = r'^(\d+)\.(\d+)\.(\d+)\.(\d+)$'
URLS = [
    'https://raw.githubusercontent.com/syoyo/tinyobjloader/master/tiny_obj_loader.h',
    'https://raw.githubusercontent.com/nothings/stb/master/stb_image.h',
    'https://raw.githubusercontent.com/nlohmann/json/develop/single_include/nlohmann/json.hpp',

    'https://github.com/glfw/glfw/releases/download/3.2.1/glfw-3.2.1.zip',
    'https://github.com/g-truc/glm/releases/download/0.9.8.5/glm-0.9.8.5.zip',
    'http://www.lua.org/ftp/lua-5.3.4.tar.gz'
]


def prefixed(url):
    return PREFIX + sep + url


def file_name(url):
    return prefixed(url.split("/")[-1])


def setup_folder():
    print_("creating folder '{}' ...".format(PREFIX), end="", flush=True)
    try:
        mkdir(PREFIX)
    except OSError as e:
        if e.errno == EEXIST:
            print_("\nfolder '{}' already exists, skipping".format(PREFIX))
    else:
        print_("done")
    print_()


def download_file(url):
    name = file_name(url)
    print_("downloading '{}' ...".format(name), end="", flush=True)
    try:
        with open(name, MODE) as fout:
            data = urllib2.urlopen(url).read()
            fout.write(data)
    except (OSError, IOError) as e:
        if e.errno == EEXIST:
            print_("\n'{}' already exists, skipping".format(name))
        else:
            print_("\nERROR downloading '{}': {}".format(name, e))
    else:
        print_("done")
    print_()


def extract_archive(name, archive_class):
    name = prefixed(name)
    print_("extracting '{}' ...".format(name), end="", flush=True)
    try:
        with archive_class(name, 'r') as archive:
            content_name = archive.namelist()[0]
            assert not exists(prefixed(content_name[:-1])), "'{}' already extracted, skipping".format(name)
            archive.extractall(PREFIX)
    except (OSError, IOError) as e:
        print_("\nERROR:", e)
    except AssertionError as e:
        print_("\n{}".format(e))
    else:
        print_("done")
    print_()


def link_sdk_folder(location):
    print_("linking sdk folder ...", location)
    sdk = prefixed(SDK_FOLDER)
    if get_system() == "Windows":
        try:
            remove(sdk)
        except OSError:
            pass
        system_command('mklink /J {} "{}"'.format(sdk, location))
        system_command('copy {0}\\Source\\lib\\vulkan-1.dll {0}\\Lib\\vulkan.dll'.format(sdk))
        system_command('copy {0}\\Source\\lib\\vulkan-1.lib {0}\\Lib\\vulkan.lib'.format(sdk))
    else:
        location = join(location, 'x86_64')
        system_command('ln -s "{}" {}'.format(location, sdk))
    print_("done")
    print_()


def find_sdk_location():
    while True:
        location = abspath(input_("Where is the 'VulkanSDK' folder located? (CTRL+C to abort): "))
        try:
            directory_list = listdir(location)
        except OSError:
            print_("ERROR: that folder does not exist")
        else:
            versions = []
            for version_name in directory_list:
                digits = match(VERSION_REGEX, version_name)
                if digits:
                    versions.append((tuple(map(int, digits.groups())), version_name))
            versions.sort(reverse=True)
            print_("found these versions:")
            for index, (number, name) in enumerate(versions, 1):
                print_("- {:>2}: '{}'".format(index, name))
            choice = input_("Enter 1 - {} or press return to use newest: ".format(len(versions))) or 1
            try:
                choice = int(choice) - 1
                choice = versions[choice]
            except (IndexError, ValueError):
                print_("ERROR: invalid choice")
            else:
                link_sdk_folder(location + sep + choice[1])
                break


def main():
    setup_folder()
    for url in URLS:
        download_file(url)
    for name in listdir(PREFIX):
        if name.endswith('.zip'):
            extract_archive(name, ZipFile)
        elif name.endswith('.tar.gz'):
            extract_archive(name, Tarfile)

    if not exists(prefixed(SDK_FOLDER)) or 'sdk' in argv:
        try:
            find_sdk_location()
        except KeyboardInterrupt:
            print_("\b\b\nstopped")


if __name__ == '__main__':
    main()
