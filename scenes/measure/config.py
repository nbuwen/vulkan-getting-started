from os.path import expanduser

VK_LOCATION = expanduser('~/vulkan/vulkan-getting-started/release/')
GL_LOCATION = expanduser('~/opengl/opengl-getting-started/release/')
