from sys import argv, exit as sys_exit, stderr
from contextlib import contextmanager
from os import mkdir
from os.path import splitext, join

from jinja2 import FileSystemLoader, Environment
from jinja2.exceptions import TemplateNotFound


@contextmanager
def ignored(*exceptions):
    try:
        yield
    except exceptions:
        pass


JINJA_OPTIONS = dict(
    block_start_string='<<',
    block_end_string='>>',
    variable_start_string='<',
    variable_end_string='>',
    comment_start_string='/*',
    comment_end_string='*/'
)


def help_exit():
    print("help: {} TEMPLATE [START] STOP [STEP]".format(argv[0]), file=stderr)
    print("    TEMPLATE: (string) name of template to use", file=stderr)
    print("    START   : (int)    start value of the range (default is 0)", file=stderr)
    print("    STOP    : (int)    stop value of the range (inclusive)", file=stderr)
    print("    STEP    : (int)    step value of the range (default is 1)", file=stderr)
    sys_exit(1)


def highest(value_range):
    return value_range.start + (len(value_range) - 1) * value_range.step


def humanify(number):
    shortcuts = [
        (1000000000, 'b'),
        (1000000, 'm'),
        (1000, 'k')
    ]

    for mod, letter in shortcuts:
        if number >= mod and number % mod == 0:
            return '{}{}'.format(number // mod, letter)

    return str(number)


def test_folder_name(test, value_range):
    name = splitext(test)[0]
    start = humanify(value_range.start)
    stop = humanify(value_range.stop - value_range.step)
    step = humanify(value_range.step)

    if step == '1':
        if start == '0':
            suffix = "-{}".format(stop)
        else:
            suffix = "-{}-{}".format(start, stop)
    else:
        suffix = "-{}-{}-{}".format(start, stop, step)

    return name + suffix


def filter_vk_correct(vector):
    return vector


def filter_gl_correct(vector):
    return [vector[0], -vector[1], -vector[2]]


def render(name, value_range):

    if not name.endswith('.json'):
        name = name + '.json'

    loader = FileSystemLoader('.')
    vk = Environment(loader=loader, **JINJA_OPTIONS)
    gl = Environment(loader=loader, **JINJA_OPTIONS)
    vk.filters['correct'] = filter_vk_correct
    gl.filters['correct'] = filter_gl_correct
    template_vk = None
    template_gl = None
    try:
        template_vk = vk.get_template(name)
        template_gl = gl.get_template(name)
    except TemplateNotFound as e:
        print("Error: template '{}' not found".format(e), file=stderr)
        help_exit()

    folder = test_folder_name(name, value_range)

    with ignored(FileExistsError):
        mkdir(folder)

    width = len(str(highest(value_range)))

    for lib, template in zip(('vk', 'gl'), (template_vk, template_gl)):
        with ignored(FileExistsError):
            mkdir(join(folder, lib))

        print("{}: 000%".format(lib), end="", flush=True)

        for i, value in enumerate(value_range, 1):
            key = str(value).zfill(width)
            filename = join(folder, lib, key) + '.json'

            with open(filename, 'w') as fout:
                fout.write(template.render(n=value, vk=lib == 'vk'))

            print("\b\b\b\b{:03}%".format(int(i / len(value_range) * 100)), end="", flush=True)
        print("\b\b\b\bdone")


def main():
    if len(argv) < 3:
        help_exit()

    template = argv[1]

    start, stop, step = None, None, None

    if len(argv) == 3:
        stop = argv[2]
    elif len(argv) == 4:
        start, stop = argv[2:4]
    elif len(argv) == 5:
        start, stop, step = argv[2:5]
    else:
        sys_exit()

    try:
        start = 0 if start is None else int(start)
        stop = stop and int(stop)
        step = 1 if step is None else int(step)
        assert step != 0, "STEP can't be 0"
        if step > 0:
            assert start <= stop, "for STEP > 0, START must not be larger than STOP"
        else:
            assert start >= stop, "for STEP < 0, START must not be smaller than STOP"
        value_range = range(start, stop + step, step)
    except (AssertionError, ValueError) as e:
        print("Error:", e)
        help_exit()
    else:
        try:
            render(template, value_range)
        except Exception as e:
            print("Error", e)


if __name__ == '__main__':
    main()
