from sys import argv, exit as sys_exit
from os import chdir
from os.path import join

from matplotlib.pyplot import show, subplots, title as set_title

from config import VK_LOCATION, GL_LOCATION


def help_exit():
    print("help: {} REPORT [XLABEL TITLE]".format(argv[0]))
    print("    REPORT: report file to use, give either vk or gl report file")
    print("    XLABEL: description of the x-axis")
    print("    TITLE : title of the plot")
    sys_exit(1)


def read_report(fin):
    for line in fin:
        line = line.split()
        for i, value in enumerate(line):
            try:
                line[i] = int(value)
            except ValueError:
                line[i] = float(value)
            yield line


def columnize(data):
    return zip(*data)


def main(args):
    title, x_label = "", ""
    if len(args) == 2:
        pass
    elif len(args) == 3:
        x_label = args[2]
    elif len(args) == 4:
        x_label, title = args[2:4]
    else:
        help_exit()

    report = args[1]
    if not report.endswith('.csv'):
        report = report + '.csv'
    vk_report = report.replace('gl', 'vk')
    gl_report = report.replace('vk', 'gl')

    vk_report = join(VK_LOCATION, vk_report)
    gl_report = join(GL_LOCATION, gl_report)

    with open(vk_report) as vin:
        header = next(vin).split()[1:]

        n, *vk_data = columnize(read_report(vin))

    with open(gl_report) as gin:
        next(gin)

        _, *gl_data = columnize(read_report(gin))

    fig, ax_fps = subplots()
    ax_fps.set_xlabel(x_label)
    ax_fps.set_title(title)
    ax_time = ax_fps.twinx()

    for ax, slice_ in zip((ax_time, ax_fps), (slice(0, 2), slice(2, 4))):
        for lib_name, lib_data in zip(('vk', 'gl'), (vk_data, gl_data)):
            for label, data in zip(header[slice_], lib_data[slice_]):
                ax.plot(n, data, label="{}-{}".format(lib_name, label))

    ax_time.legend()
    ax_fps.legend(loc="upper left")

    ax_time.set_ylabel("$Frame Time (\\frac{1}{s})$")
    ax_fps.set_ylabel("$Frame Rate (s)$")

    show()


if __name__ == '__main__':
    main(argv)
