from re import match
from sys import argv, stderr, exit as sys_exit
from os import listdir, chdir
from os.path import join, splitext
from subprocess import Popen, PIPE
from statistics import mean, median

from config import GL_LOCATION, VK_LOCATION

REPORT_LINE = r'FrameTime:\s+([0-9\.eE-]+)\s+-\s+FPS\s+([0-9]+)'
LOCATION = dict(vk=VK_LOCATION, gl=GL_LOCATION)
TEST_FOLDER = "{test}/{lib}/"


def make_args(lib, test):
    return LOCATION[lib], TEST_FOLDER.format(test=test, lib=lib)


def help_exit():
    print("help: {} LIB TEST".format(argv[0]), file=stderr)
    print("        LIB  : either 'vk' for vulkan or 'gl' for opengl")
    print("        TEST : the test run")
    sys_exit(1)


def extract_data_from_line(line):
    data = match(REPORT_LINE, line)

    if data:
        time, fps = data.groups()
    else:
        print("failed to parse line:", line)
        return None

    return float(time), int(fps)


def progress(current, max_):
    back = ('\b' * (max_ + 3)) if current else ''
    done = '=' * current
    missing = '-' * (max_ - current)
    print("{}[{}{}] ".format(back, done, missing), end="", flush=True)


def run(scene, steps):
    progress(0, steps)
    with Popen(['./base', scene], stdout=PIPE, universal_newlines=True) as process:
        for i, line in enumerate(process.stdout, 1):
            result = extract_data_from_line(line)
            if result is not None:
                yield result

                progress(i, steps)
                if i == steps:
                    break
    print()


def main(args):
    if len(args) != 3:
        help_exit()

    lib, test = args[1:3]
    test = test.rstrip('/')

    print("TEST", test)

    if lib not in ('vk', 'gl'):
        print("unknown argument for LIB", file=stderr)
        help_exit()

    working_directory, folder = make_args(lib, test)
    folders = sorted(listdir(folder))
    chdir(working_directory)

    result = []

    try:
        for number in folders:
            scene = join('measure', folder, splitext(number)[0])
            print("{} / {}: ".format(number[:-5], folders[-1][:-5]), end="")
            data = list(run(scene, 10))
            try:
                time, fps = zip(*data)
            except ValueError:
                time, fps = [0], [0]
            result.append((mean(time), median(time), mean(fps), median(fps)))
    except KeyboardInterrupt:
        print("stopped")

    with open('report-{}-{}.csv'.format(lib, test), 'w') as fout:
        fout.write('n          mean(t)         median(t)       mean(fps)       median(fps)\n')
        for line, number in zip(result, folders):
            fout.write('{:<10} {:<15} {:<15} {:<15} {:<9}\n'.format(splitext(number)[0], *line))
        fout.write('\n')


if __name__ == '__main__':
    main(argv)
