#ifndef TRIANGLE_APP_H
#define TRIANGLE_APP_H


#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <json.hpp>
using json = nlohmann::json;

#include <vector>
#include <set>
#include <array>
#include <chrono>
#include <unordered_map>

#include "camera.h"
#include "instance.h"
#include "object.h"
#include "swapchain.h"
#include "pipelines.h"
#include "arrayobject.h"
#include "lua.h"


class App
{
public:
    void init();
    bool shouldRun() const;
    float loop();
    void shutdown();
    App(uint32_t width, uint32_t height, bool enableValidation, json scene);
    virtual ~App();
private:
    void initWindow();
    void initVulkan();
    void createInstance();
    void createSurface();
    void createDebugCallback();
    void findPhysicalDevice();
    float getDeviceScore(VkPhysicalDevice device);
    void createLogicalDevice();
    void createRenderPass();
    void createFramebuffers();
    void createCommandPool();
    void updatePushConstants();
    void createDrawCommandBuffers();
    void createPushCommandBuffers();
    void recordDrawCommands();
    void recordPushCommands();
    void submitPushCommands();
    void createSemaphores();
    void createDepthResources();
    void loadObjects();
    const Model& loadModel(const std::string& filename);
    const Texture& loadTexture(const std::string& filename);
    std::string loadLuaScript(const std::string& script);

    void cleanSwapChain();
    void recreateSwapChain();

    void drawFrame();
    void updateUniformBuffers();

    static void onWindowResized(GLFWwindow* window, int width, int height);
    static void onFocusChanged(GLFWwindow* window, int focused);

    Instance instance;

    ArrayObject array;

    struct {
        std::vector<Object> objects;
        std::unordered_map<std::string, Texture> textures;
        std::unordered_map<std::string, Model> models;
        json config;

        bool useSkybox;
        bool forceFifo;
    } scene;

    struct {
        std::vector<UniformBufferLocal> uniformLocal;
        PushConstants pushConstants;
    } data;

    Swapchain swapchain;
    Pipelines pipelines;

    std::vector<const char*> validationLayers = {
            "VK_LAYER_LUNARG_core_validation",
            "VK_LAYER_LUNARG_parameter_validation",
            "VK_LAYER_LUNARG_monitor",
            "VK_LAYER_LUNARG_assistant_layer"
    };
    std::vector<const char*> deviceExtensions = {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };

    Camera camera;
    bool enableValidation;
    uint32_t width;
    uint32_t height;

    GLFWwindow* window = nullptr;

    VkDebugReportCallbackEXT debugCallback = VK_NULL_HANDLE;



    VkRenderPass renderPass = VK_NULL_HANDLE;
    std::vector<VkFramebuffer> framebuffers;

    struct
    {
        std::vector<VkCommandBuffer> draw;
        std::array<VkCommandBuffer, 3> push;
        size_t currentPush = 0;
    } commandBuffers;


    VkSemaphore imageAvailable = VK_NULL_HANDLE;
    VkSemaphore renderFinished = VK_NULL_HANDLE;

    VkImage depthImage = VK_NULL_HANDLE;
    VkDeviceMemory depthMemory = VK_NULL_HANDLE;
    VkImageView depthView = VK_NULL_HANDLE;

    std::chrono::high_resolution_clock::time_point startTime;
    std::chrono::high_resolution_clock::time_point totalStartTime;
    float totalTime;
    float deltaTime;

    struct {
        int scriptCount = 0;
        Lua interpreter = {};
    } lua;

};


#endif //TRIANGLE_APP_H
