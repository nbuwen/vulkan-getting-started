#ifndef VULKANGETTINGSTARTED_INSTANCE_H
#define VULKANGETTINGSTARTED_INSTANCE_H

#include <set>

#include <vulkan/vulkan.h>

#include "model.h"

struct Instance;
struct Model;
struct Vertex;

struct QueueFamilies
{
    int graphics = -1;
    int presentation = -1;

    bool isComplete() const;
    std::set<int> uniqueFamilies() const;

    static QueueFamilies find(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface);
    static QueueFamilies find(const Instance& instance);
};

struct Instance
{
    VkInstance instance = VK_NULL_HANDLE;
    VkDevice device = VK_NULL_HANDLE;
    VkCommandPool pool = VK_NULL_HANDLE;
    VkSurfaceKHR surface = VK_NULL_HANDLE;

    struct {
        VkQueue graphics = VK_NULL_HANDLE;
        VkQueue presentation = VK_NULL_HANDLE;
    } queues;

    VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
    VkPhysicalDeviceProperties properties;

	Instance();
    Instance(const Instance&) = delete;
    Instance(Instance&&) = delete;
    Instance& operator=(const Instance&) = delete;
    Instance& operator=(Instance&&) = delete;

    const VkInstance& get() const;
    VkInstance* set();
    void clean();
};


#endif //VULKANGETTINGSTARTED_INSTANCE_H
