#include <stdexcept>
#include <iostream>
#include <vector>

#include "vkutil.h"
#include "enums.h"

bool hasError(VkResult result)
{
    if(result != VK_SUCCESS)
    {
        std::clog << vulkanResult(result) << std::endl;
        return true;
    }
    return false;
}

uint32_t findMemoryType(uint32_t filter, VkMemoryPropertyFlags flags, VkPhysicalDevice physicalDevice)
{
    VkPhysicalDeviceMemoryProperties memory = {};
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memory);

    for(uint32_t i = 0; i < memory.memoryTypeCount; ++i)
    {
        if(filter & (1 << i) &&
           (memory.memoryTypes[i].propertyFlags & flags) == flags)
        {
            return i;
        }
    }

    throw std::runtime_error("Failed to find suitable memory type");
}

void createBuffer(const Instance& instance,
                  VkDeviceSize size,
                  VkBufferUsageFlags usageFlags,
                  VkMemoryPropertyFlags memoryPropertyFlags,
                  VkBuffer &buffer,
                  VkDeviceMemory &memory)
{
    VkBufferCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    create.size = size;
    create.usage = usageFlags;
    create.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if(hasError(vkCreateBuffer(instance.device, &create, nullptr, &buffer)))
    {
        throw std::runtime_error("Failed to create buffer");
    }

    VkMemoryRequirements memoryRequirements = {};
    vkGetBufferMemoryRequirements(instance.device, buffer, &memoryRequirements);

    VkMemoryAllocateInfo allocate = {};
    allocate.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocate.allocationSize = memoryRequirements.size;
    allocate.memoryTypeIndex = findMemoryType(memoryRequirements.memoryTypeBits, memoryPropertyFlags, instance.physicalDevice);

    if(hasError(vkAllocateMemory(instance.device, &allocate, nullptr, &memory)))
    {
        throw std::runtime_error("Failed to allocate buffer memory");
    }

    vkBindBufferMemory(instance.device, buffer, memory, 0);
}

void transitionImageLayout(const Instance& instance,
                           VkImage image,
                           VkFormat format,
                           VkImageLayout oldLayout,
                           VkImageLayout newLayout,
                           bool cubeMap)
{
    VkCommandBuffer command = beginSingleTimeCommands(instance);

    VkImageMemoryBarrier barrier = {};
    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = oldLayout;
    barrier.newLayout = newLayout;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;

    if(newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
        if(hasStencilComponent(format))
        {
            barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
        }
    }
    else
    {
        barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    }

    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = 1;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = cubeMap? 6 : 1;

    VkPipelineStageFlags source;
    VkPipelineStageFlags destination;

    if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        source = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destination = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if(oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
        barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        source = VK_PIPELINE_STAGE_TRANSFER_BIT;
        destination = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else if(oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
    {
        barrier.srcAccessMask = 0;
        barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

        source = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        destination = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    }
    else
    {
        throw std::invalid_argument("Unsupported layout transition");
    }

    vkCmdPipelineBarrier(command, source, destination, 0, 0, nullptr, 0, nullptr, 1, &barrier);

    endSingleTimeCommands(instance, command);
}

VkCommandBuffer beginSingleTimeCommands(const Instance& instance)
{
    VkCommandBufferAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = instance.pool;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(instance.device, &allocInfo, &commandBuffer);

    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    vkBeginCommandBuffer(commandBuffer, &beginInfo);

    return commandBuffer;
}

void endSingleTimeCommands(const Instance& instance, VkCommandBuffer commandBuffer)
{
    vkEndCommandBuffer(commandBuffer);

    VkSubmitInfo submitInfo = {};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    vkQueueSubmit(instance.queues.graphics, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(instance.queues.graphics);

    vkFreeCommandBuffers(instance.device, instance.pool, 1, &commandBuffer);
}

bool hasStencilComponent(VkFormat format)
{
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

void createImage(const Instance& instance,
                 uint32_t width,
                 uint32_t height,
                 VkImageCreateFlags flags,
                 VkFormat format,
                 VkImageTiling tiling,
                 VkImageUsageFlags usage,
                 VkMemoryPropertyFlags properties,
                 VkImage &image,
                 VkDeviceMemory &memory)
{
    VkImageCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    create.flags = flags;
    create.imageType = VK_IMAGE_TYPE_2D;
    create.extent.width = width;
    create.extent.height = height;
    create.extent.depth = 1;
    create.mipLevels = 1;
    create.arrayLayers = (flags & VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT)? 6 : 1;
    create.format = format;
    create.tiling = tiling;
    create.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    create.usage = usage;
    create.samples = VK_SAMPLE_COUNT_1_BIT;
    create.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if (vkCreateImage(instance.device, &create, nullptr, &image) != VK_SUCCESS) {
        throw std::runtime_error("Failed to create image");
    }

    VkMemoryRequirements memRequirements = {};
    vkGetImageMemoryRequirements(instance.device, image, &memRequirements);

    VkMemoryAllocateInfo allocInfo = {};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits,
                                               properties,
                                               instance.physicalDevice);

    if (vkAllocateMemory(instance.device, &allocInfo, nullptr, &memory) != VK_SUCCESS) {
        throw std::runtime_error("Failed to allocate image memory");
    }

    vkBindImageMemory(instance.device, image, memory, 0);
}

VkImageView createImageView(const Instance& instance,
                            VkImage image,
                            VkImageViewType type,
                            VkFormat format,
                            VkImageAspectFlags flags)
{
    VkImageViewCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    create.image = image;
    create.viewType = type;
    create.format = format;
    create.subresourceRange.aspectMask = flags;
    create.subresourceRange.baseMipLevel = 0;
    create.subresourceRange.levelCount = 1;
    create.subresourceRange.baseArrayLayer = 0;
    create.subresourceRange.layerCount = (type == VK_IMAGE_VIEW_TYPE_CUBE)? 6 : 1;

    VkImageView imageView;
    if (vkCreateImageView(instance.device, &create, nullptr, &imageView) != VK_SUCCESS) {
        throw std::runtime_error("failed to create texture image view!");
    }

    return imageView;
}

void copyBufferToCube(const Instance& instance,
                      VkBuffer buffer,
                      VkImage image,
                      uint32_t width,
                      uint32_t height)
{
    VkCommandBuffer command = beginSingleTimeCommands(instance);

    std::vector<VkBufferImageCopy> copies;
    uint32_t offset = 0;

    for(uint32_t face = 0; face < 6; ++face)
    {
        VkBufferImageCopy copy = {};
        copy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        copy.imageSubresource.mipLevel = 0;
        copy.imageSubresource.baseArrayLayer = face;
        copy.imageSubresource.layerCount = 1;
        copy.imageExtent = {width, height, 1};
        copy.bufferOffset = offset;
        copies.push_back(copy);

        offset += width * height * 4;
    }

    vkCmdCopyBufferToImage(command, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, (uint32_t)copies.size(), copies.data());

    endSingleTimeCommands(instance, command);
}

void copyBufferToImage(const Instance& instance,
                       VkBuffer buffer,
                       VkImage image,
                       uint32_t width,
                       uint32_t height)
{
    VkCommandBuffer command = beginSingleTimeCommands(instance);

    VkBufferImageCopy copy = {};
    copy.bufferOffset = 0;
    copy.bufferRowLength = 0;
    copy.bufferImageHeight = 0;
    copy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    copy.imageSubresource.mipLevel = 0;
    copy.imageSubresource.baseArrayLayer = 0;
    copy.imageSubresource.layerCount = 1;
    copy.imageOffset = {0, 0, 0};
    copy.imageExtent = {width, height, 1};

    vkCmdCopyBufferToImage(command, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy);

    endSingleTimeCommands(instance, command);
}

void copyBuffer(const Instance& instance, VkBuffer src, VkBuffer dst, VkDeviceSize size)
{
    VkCommandBuffer  command = beginSingleTimeCommands(instance);

    VkBufferCopy copy = {};
    copy.size = size;
    vkCmdCopyBuffer(command, src, dst, 1, &copy);
    endSingleTimeCommands(instance, command);
}