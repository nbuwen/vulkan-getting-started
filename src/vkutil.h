#ifndef VULKANGETTINGSTARTED_VKUTIL_H
#define VULKANGETTINGSTARTED_VKUTIL_H

#include <vulkan/vulkan.h>

#include "instance.h"

bool hasError(VkResult result);
uint32_t findMemoryType(uint32_t filter, VkMemoryPropertyFlags flags, VkPhysicalDevice physicalDevice);
bool hasStencilComponent(VkFormat format);

void createBuffer(const Instance& instance,
                  VkDeviceSize size,
                  VkBufferUsageFlags usageFlags,
                  VkMemoryPropertyFlags memoryPropertyFlags,
                  VkBuffer& buffer,
                  VkDeviceMemory& memory);

void createImage(const Instance& instance,
                 uint32_t width,
                 uint32_t height,
                 VkImageCreateFlags flags,
                 VkFormat format,
                 VkImageTiling tiling,
                 VkImageUsageFlags usage,
                 VkMemoryPropertyFlags properties,
                 VkImage &image,
                 VkDeviceMemory &memory);

VkImageView createImageView(const Instance& instance,
                            VkImage image,
                            VkImageViewType type,
                            VkFormat format,
                            VkImageAspectFlags flags);

void transitionImageLayout(const Instance&, VkImage image,
                           VkFormat format,
                           VkImageLayout oldLayout,
                           VkImageLayout newLayout,
                           bool cubeMap);

void copyBufferToImage(const Instance& instance,
                       VkBuffer buffer,
                       VkImage image,
                       uint32_t width,
                       uint32_t height);

void copyBufferToCube(const Instance& instance,
                      VkBuffer buffer,
                      VkImage image,
                      uint32_t width,
                      uint32_t height);

void copyBuffer(const Instance& instance, VkBuffer src, VkBuffer dst, VkDeviceSize size);

VkCommandBuffer beginSingleTimeCommands(const Instance& instance);
void endSingleTimeCommands(const Instance& instance, VkCommandBuffer commandBuffer);

#endif //VULKANGETTINGSTARTED_VKUTIL_H
