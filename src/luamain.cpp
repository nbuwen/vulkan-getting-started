#include <iostream>
#include <cassert>

#include <glm/glm.hpp>

#include "lua.h"


std::ostream& operator<< (std::ostream& out, const glm::vec3& vec)
{
    return out << "[" << vec.x << ", " << vec.y << ", " << vec.z << "]";
}

std::ostream& operator<< (std::ostream& out, const glm::vec2& vec)
{
    return out << "[" << vec.x << ", " << vec.y << "]";
}


void test()
{
    glm::vec2 x = {123.4f, 234.5f};
    glm::vec3 y = {12.3f, 23.4f, 34.5f};
    glm::vec4 z = {1.2f, 2.3f, 3.4f, 4.5f};
    
    glm::vec2 a = {0.0f, 0.0f};
    glm::vec3 b = {0.0f, 0.0f, 0.0f};
    glm::vec4 c = {0.0f, 0.0f, 0.0f, 0.0f};

    Lua lua;
    lua.loadFile("test/test_wrapper");

    lua.loadFunction("TestUnary");

    lua.pushv(x);
    lua.pushv(y);
    lua.pushv(z);

    lua.call(3, 0);

    assert(x.x == -1000.2f && x.y == -2000.3f);
    assert(y.x == -100.2f && y.y == -200.3f && y.z == -300.4f);
    assert(z.x == -10.2f && z.y == -20.3f && z.z == -30.4f && z.w == -40.5f);
    
    lua.loadFunction("TestBinary");

    lua.pushv(x);
    lua.pushv(a);
    lua.pushv(y);
    lua.pushv(b);
    lua.pushv(z);
    lua.pushv(c);

    lua.call(6, 0);
    
    std::cout << "All tests passed" << std::endl << std::endl;
}

int main()
{
    test();
}