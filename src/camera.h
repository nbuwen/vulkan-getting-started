#ifndef TRIANGLE_CAMERA_H
#define TRIANGLE_CAMERA_H

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <json.hpp>


class Camera
{
    nlohmann::json& json;

    bool _inactive = true;

    glm::vec2 _angle = {0, 0};
    glm::vec2 _turnSpeed = {-0.01, -0.01};

    glm::vec3 _forward = {0, 0, 1};
    glm::vec3 _right = {1, 0, 0};
    glm::vec3 _up = {0, -1, 0};

    glm::vec3 _speed = {1, 1, 1};

    struct {
        glm::mat4 matrix;
        bool isPerspective;
        bool firstUpdate = true;
        float perspectiveFov = 60.0f;
        float orthographicSize = 5.0f;
    } _projection;

    void setPerspective(float ratio);
    void setOrthographic();
public:
    glm::vec3 position;

    void disable(bool disabled);
    void update(GLFWwindow* window, float deltaTime);
    glm::mat4 view() const;
    const glm::mat4& projection() const;

    explicit Camera(nlohmann::json& json);
};

#endif //TRIANGLE_CAMERA_H
