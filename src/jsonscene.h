#ifndef VULKANGETTINGSTARTED_JSON_H
#define VULKANGETTINGSTARTED_JSON_H

#include <json.hpp>

nlohmann::json loadScene(const std::string& filename);

#endif //VULKANGETTINGSTARTED_JSON_H
