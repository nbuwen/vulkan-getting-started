#ifndef TRIANGLE_EXTENSIONLOADER_H
#define TRIANGLE_EXTENSIONLOADER_H

#include <vulkan/vulkan.h>

VkResult vCreateDebugReportCallback(
        VkInstance instance,
        const VkDebugReportCallbackCreateInfoEXT* create,
        const VkAllocationCallbacks* allocator,
        VkDebugReportCallbackEXT* callback
);

void vDestroyDebugReportCallback(
        VkInstance instance,
        VkDebugReportCallbackEXT callback,
        const VkAllocationCallbacks* allocator
);

#endif //TRIANGLE_EXTENSIONLOADER_H
