#include <vector>

#include "instance.h"

VkInstance *Instance::set()
{
    if(instance == VK_NULL_HANDLE)
    {
        return &instance;
    }
    throw std::runtime_error("Failed to set instance. It is already set");
}

Instance::Instance()
{
	
}

const VkInstance &Instance::get() const
{
    return instance;
}

void Instance::clean()
{
    vkDestroyDevice(device, nullptr);
    vkDestroySurfaceKHR(instance, surface, nullptr);
    vkDestroyInstance(instance, nullptr);
}

QueueFamilies QueueFamilies::find(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface)
{
    QueueFamilies choice;

    uint32_t size = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &size, nullptr);
    std::vector<VkQueueFamilyProperties> families(size);
    vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &size, families.data());

    uint32_t index = 0;
    for(const auto& family : families)
    {
        VkBool32 canPresent = VK_FALSE;
        vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice,
                                             index,
                                             surface,
                                             &canPresent);
        if(family.queueCount > 0 && family.queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            choice.graphics = index;
        }
        if(family.queueCount > 0 && canPresent)
        {
            choice.presentation = index;
        }
        if(choice.isComplete())
        {
            break;
        }
        ++index;
    }

    return choice;
}

bool QueueFamilies::isComplete() const
{
    return graphics >= 0 && presentation >= 0;
}

std::set<int> QueueFamilies::uniqueFamilies() const
{
    return {graphics, presentation};
}

QueueFamilies QueueFamilies::find(const Instance &instance)
{
    return QueueFamilies::find(instance.physicalDevice, instance.surface);
}

