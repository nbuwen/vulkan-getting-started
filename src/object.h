#ifndef VULKANGETTINGSTARTED_OBJECT_H
#define VULKANGETTINGSTARTED_OBJECT_H

#include <json.hpp>

#include "model.h"
#include "texture.h"

struct Object
{
    const Model& model;
    const Texture& texture;

    uint32_t firstInstance;
    uint32_t instanceCount;
    uint32_t pipelineIndex;

    std::string luaKey;

    glm::vec3 position;
    glm::vec3 scale;
    glm::vec3 rotation;
    glm::vec3 color;

    Object(const Model& model, const Texture& texture, const nlohmann::json& object);
    Object(const Model& model, const Texture& texture, uint32_t pipelineIndex);
};


#endif //VULKANGETTINGSTARTED_OBJECT_H
