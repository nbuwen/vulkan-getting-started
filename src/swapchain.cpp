#include "swapchain.h"
#include "vkutil.h"

VkSurfaceFormatKHR findSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& available)
{
    if(available.size() == 1 && available[0].format == VK_FORMAT_UNDEFINED)
    {
        return {VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR};
    }
    for(const auto& format : available)
    {
        if(format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            return format;
        }
    }

    return available[0];
}

VkPresentModeKHR findSwapPresentMode(const std::vector<VkPresentModeKHR>& available)
{
    auto best = VK_PRESENT_MODE_FIFO_KHR;

    for(const auto& mode : available)
    {
        if(mode == VK_PRESENT_MODE_MAILBOX_KHR)
        {
            return mode;
        }
        if(mode == VK_PRESENT_MODE_IMMEDIATE_KHR)
        {
            best = mode;
        }
    }
    return best;
}

VkExtent2D findSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities, int width, int height)
{
    if(capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max())
    {
        return capabilities.currentExtent;
    }

    VkExtent2D extent = {};
    extent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, (uint32_t)width));
    extent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, (uint32_t)height));

    return extent;
}

void Swapchain::create(const Instance &instance, int width, int height, bool forceFifo)
{
    VkSurfaceCapabilitiesKHR capabilities = {};
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(instance.physicalDevice, instance.surface, &capabilities);

    uint32_t size = 0;
    vkGetPhysicalDeviceSurfaceFormatsKHR(instance.physicalDevice, instance.surface, &size, nullptr);
    std::vector<VkSurfaceFormatKHR> formats(size);
    vkGetPhysicalDeviceSurfaceFormatsKHR(instance.physicalDevice, instance.surface, &size, formats.data());

    size = 0;
    vkGetPhysicalDeviceSurfacePresentModesKHR(instance.physicalDevice, instance.surface, &size, nullptr);
    std::vector<VkPresentModeKHR> modes(size);
    vkGetPhysicalDeviceSurfacePresentModesKHR(instance.physicalDevice, instance.surface, &size, modes.data());

    auto surfaceFormat = findSwapSurfaceFormat(formats);
    auto presentMode = forceFifo? VK_PRESENT_MODE_FIFO_KHR : findSwapPresentMode(modes);
    extent = findSwapExtent(capabilities, width, height);

    uint32_t imageCount = capabilities.minImageCount + 1;
    if(capabilities.maxImageCount > 0 && imageCount > capabilities.maxImageCount)
    {
        imageCount = capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR create = {};
    create.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    create.surface = instance.surface;
    create.minImageCount = imageCount;
    create.imageFormat = surfaceFormat.format;
    create.imageColorSpace = surfaceFormat.colorSpace;
    create.imageExtent = extent;
    create.imageArrayLayers = 1;
    create.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    QueueFamilies families = QueueFamilies::find(instance);
    if(families.presentation != families.graphics)
    {
        create.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        create.queueFamilyIndexCount = 2;
        uint32_t indices[] = {(uint32_t)families.presentation, (uint32_t)families.graphics};
        create.pQueueFamilyIndices = indices;
    }
    else
    {
        create.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        create.queueFamilyIndexCount = 0;
        create.pQueueFamilyIndices = nullptr;
    }

    create.preTransform = capabilities.currentTransform;
    create.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    create.presentMode = presentMode;
    create.clipped = VK_TRUE;
    create.oldSwapchain = VK_NULL_HANDLE;

    if(hasError(vkCreateSwapchainKHR(instance.device, &create, nullptr, &handle)))
    {
        throw std::runtime_error("Failed to create swap chain");
    }

    vkGetSwapchainImagesKHR(instance.device, handle, &imageCount, nullptr);
    images.resize(imageCount);
    vkGetSwapchainImagesKHR(instance.device, handle, &imageCount, images.data());

    format = surfaceFormat.format;

    views.resize(imageCount);
    for(size_t i = 0; i < imageCount; ++i)
    {
        views[i] = createImageView(instance, images[i], VK_IMAGE_VIEW_TYPE_2D, format, VK_IMAGE_ASPECT_COLOR_BIT);
    }

}

const VkSwapchainKHR &Swapchain::get() const
{
    return handle;
}

void Swapchain::clean(const Instance &instance)
{
    for(auto& view : views)
    {
        vkDestroyImageView(instance.device, view, nullptr);
    }
    vkDestroySwapchainKHR(instance.device, handle, nullptr);
}
