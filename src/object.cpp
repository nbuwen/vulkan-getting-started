#include "object.h"
#include "util.h"

Object::Object(const Model &model,
               const Texture &texture,
               const nlohmann::json& object)
    : model(model),
      texture(texture),
      firstInstance(object["firstInstance"].get<int>()),
      instanceCount(object["instances"].get<int>()),
      pipelineIndex(object["pipeline"].get<int>()),
      luaKey(object["luaKey"].get<std::string>()),
      position(jsonToVec3(object["position"])),
      scale(jsonToVec3(object["scale"])),
      rotation(jsonToVec3(object["rotation"])),
      color(jsonToVec3(object["color"]))
{

}

Object::Object(const Model &model, const Texture &texture, uint32_t pipelineIndex)
        : model(model),
          texture(texture),
          firstInstance(0),
          instanceCount(1),
          pipelineIndex(pipelineIndex)
{

}

