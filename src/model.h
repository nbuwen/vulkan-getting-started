#ifndef VULKANGETTINGSTARTED_MODEL_H
#define VULKANGETTINGSTARTED_MODEL_H

#include <cstddef>
#include <string>
#include <array>

#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>

#include "instance.h"
#include "arrayobject.h"

struct Instance;
struct ArrayObject;

struct Model
{
    uint32_t offset = 0;
    uint32_t size = 0;

    void load(ArrayObject& array,  const std::string &filename);
};

struct Vertex
{
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec3 color;
    glm::vec2 texCoord;

    bool operator==(const Vertex& vertex) const;
    static VkVertexInputBindingDescription binding();
    static std::array<VkVertexInputAttributeDescription, 4> attributes();

    struct Hasher
    {
        std::size_t operator()(const Vertex& vertex) const;
    };
};


#endif //VULKANGETTINGSTARTED_MODEL_H
