#ifndef VULKANGETTINGSTARTED_TEXTURE_H
#define VULKANGETTINGSTARTED_TEXTURE_H

#include <string>

#include <vulkan/vulkan.h>
#include <json.hpp>

#include "instance.h"
#include "pipelines.h"

struct Pipelines;

struct Texture
{
    VkImage image = VK_NULL_HANDLE;
    VkImageView view = VK_NULL_HANDLE;
    VkDeviceMemory memory = VK_NULL_HANDLE;
    VkDescriptorSet descriptorSet = VK_NULL_HANDLE;

    void load(const Instance &instance, const std::string &filename);
    void loadSkybox(const Instance &instance, const nlohmann::json &box);
    void createDescriptorSet(const Instance& instance, const Pipelines& pipelines);
    void clean(const Instance& instance);
};

#endif
