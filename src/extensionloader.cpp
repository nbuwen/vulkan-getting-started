#include "extensionloader.h"


VkResult vCreateDebugReportCallback(
        VkInstance instance,
        const VkDebugReportCallbackCreateInfoEXT* create,
        const VkAllocationCallbacks* allocator,
        VkDebugReportCallbackEXT* callback
)
{
    auto func = (PFN_vkCreateDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
    if(func != nullptr)
    {
        return func(instance, create, allocator, callback);
    }
    else
    {
        return VK_ERROR_EXTENSION_NOT_PRESENT;
    }
}

void vDestroyDebugReportCallback(
        VkInstance instance,
        VkDebugReportCallbackEXT callback,
        const VkAllocationCallbacks* allocator
)
{
    auto func = (PFN_vkDestroyDebugReportCallbackEXT) vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
    if(func != nullptr)
    {
        func(instance, callback, allocator);
    }
}