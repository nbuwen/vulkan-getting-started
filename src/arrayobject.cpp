#include <cstring>

#include "arrayobject.h"
#include "vkutil.h"

void ArrayObject::create(const Instance &instance)
{
    VkDeviceSize size = sizeof(Vertex) * vertexData.size();
    VkBuffer staging;
    VkDeviceMemory stagingMemory;
    createBuffer(instance,
                 size,
                 VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                 staging,
                 stagingMemory);

    void* data;
    vkMapMemory(instance.device, stagingMemory, 0, size, 0, &data);
    memcpy(data, vertexData.data(), size);
    vkUnmapMemory(instance.device, stagingMemory);

    createBuffer(instance,
                 size,
                 VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                 vertexBuffer,
                 vertexMemory);

    copyBuffer(instance, staging, vertexBuffer, size);
    vkDestroyBuffer(instance.device, staging, nullptr);
    vkFreeMemory(instance.device, stagingMemory, nullptr);
    vertexData.clear();

    size = sizeof(uint32_t) * indexData.size();
    createBuffer(instance,
                 size,
                 VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                 staging,
                 stagingMemory);

    vkMapMemory(instance.device, stagingMemory, 0, size, 0, &data);
    memcpy(data, indexData.data(), size);
    vkUnmapMemory(instance.device, stagingMemory);

    createBuffer(instance,
                 size,
                 VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
                 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                 indexBuffer,
                 indexMemory);
    copyBuffer(instance, staging, indexBuffer, size);
    vkDestroyBuffer(instance.device, staging, nullptr);
    vkFreeMemory(instance.device, stagingMemory, nullptr);
    indexData.clear();
}

void ArrayObject::clean(const Instance &instance)
{
    if(indexBuffer != VK_NULL_HANDLE) vkDestroyBuffer(instance.device, indexBuffer, nullptr);
    if(indexMemory != VK_NULL_HANDLE) vkFreeMemory(instance.device, indexMemory, nullptr);
    if(vertexBuffer != VK_NULL_HANDLE) vkDestroyBuffer(instance.device, vertexBuffer, nullptr);
    if(vertexMemory != VK_NULL_HANDLE) vkFreeMemory(instance.device, vertexMemory, nullptr);
}
