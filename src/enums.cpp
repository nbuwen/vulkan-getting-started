#include "enums.h"


std::string vulkanResult(VkResult result)
{
    switch(result)
    {
    case VK_SUCCESS:
        return "Success";
    case VK_ERROR_OUT_OF_DEVICE_MEMORY:
        return "Error: out of device memory";
    case VK_ERROR_OUT_OF_HOST_MEMORY:
        return "ERROR: out of host memory";
    case VK_ERROR_LAYER_NOT_PRESENT:
        return "Error: layer not present" ;
    case VK_ERROR_DEVICE_LOST:
        return "Error: device lost" ;
    case VK_ERROR_EXTENSION_NOT_PRESENT:
        return "Error: extension not present" ;
    case VK_ERROR_FEATURE_NOT_PRESENT:
        return "Error: feature not present" ;
    case VK_ERROR_FORMAT_NOT_SUPPORTED:
        return "Error: format not present" ;
    case VK_ERROR_FRAGMENTED_POOL:
        return "Error: fragmented pool" ;
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
        return "Error: incompatible display" ;
    case VK_ERROR_INCOMPATIBLE_DRIVER:
        return "Error: incompatible driver" ;
    case VK_ERROR_INITIALIZATION_FAILED:
        return "Error: linitialization failed" ;
    case VK_ERROR_INVALID_EXTERNAL_HANDLE_KHR:
        return "Error: invalid external handle" ;
    case VK_ERROR_INVALID_SHADER_NV:
        return "Error: invalid shader nv" ;
    case VK_ERROR_MEMORY_MAP_FAILED:
        return "Error: memory map failed" ;
    case VK_ERROR_VALIDATION_FAILED_EXT:
        return "Error: validation failed";
    case VK_ERROR_TOO_MANY_OBJECTS:
        return "Error: too many objects";
    case VK_ERROR_SURFACE_LOST_KHR:
        return "Error: surface lost KHR";
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
        return "Error: native window in use KHR";
    case VK_ERROR_OUT_OF_DATE_KHR:
        return "Error: out of date KHR";
    case VK_ERROR_OUT_OF_POOL_MEMORY_KHR:
        return "Error: out of pool memory KHR";
    default:
        return "Error: unknown error: " + std::to_string(result);
    }
}

std::string physicalDeviceType(VkPhysicalDeviceType type)
{
    switch(type)
    {
    case VK_PHYSICAL_DEVICE_TYPE_OTHER:
        return "Other";
    case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU:
        return "Integrated";
    case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
        return "Discrete";
    case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU:
        return "Virtual";
    case VK_PHYSICAL_DEVICE_TYPE_CPU:
        return "CPU";

    default:
        return "Unknown";
    }
}

std::string unpackVersion(uint32_t vulkanVersion)
{
    uint32_t major = VK_VERSION_MAJOR(vulkanVersion);
    uint32_t minor = VK_VERSION_MINOR(vulkanVersion);
    uint32_t patch = VK_VERSION_PATCH(vulkanVersion);
    return std::to_string(major) + '.' + std::to_string(minor) + '.' + std::to_string(patch);
}

int hex(int i)
{
    if(i > 9)
    {
        return 'A' + i - 10;
    }
    return '0' + i;
}

std::string uuidToHex(const uint8_t* begin, size_t size)
{
    std::string result;
    for(size_t i = 0; i < size; ++i)
    {
        result += (char)hex(begin[i] / 16);
        result += (char)hex(begin[i] % 16);
        if(i < size - 1)
        {
            result += '-';
        }
    }
    return result;
}