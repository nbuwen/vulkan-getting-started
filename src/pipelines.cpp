#include <fstream>

#include <json.hpp>

#include "pipelines.h"
#include "vkutil.h"
#include "app.h"

static std::vector<char> readFile(const std::string& filename)
{
    std::ifstream fin(filename, std::ios::ate | std::ios::binary);
    if(!fin.is_open())
    {
        std::string errorMessage = "Failed to open file " + filename;
        throw std::runtime_error(errorMessage);
    }
    auto size = (size_t)fin.tellg();
    std::vector<char> buffer(size);
    fin.seekg(0);
    fin.read(buffer.data(), size);

    fin.close();
    return buffer;
}

std::string shaderExtension(VkShaderStageFlagBits stage)
{
    switch(stage)
    {
    case VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT:
        return ".tese.spv";
    case VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT:
        return ".tesc.spv";
    case VK_SHADER_STAGE_FRAGMENT_BIT:
        return ".frag.spv";
    case VK_SHADER_STAGE_VERTEX_BIT:
        return ".vert.spv";
    case VK_SHADER_STAGE_COMPUTE_BIT:
    case VK_SHADER_STAGE_GEOMETRY_BIT:
        throw std::runtime_error("Failed to create shader: geometry and compute shaders are not supported");
    default:
        throw std::runtime_error("Failed to create shader: this shader stage does not have a single extension");
    }
}

void createShader(const Instance& instance,
                  const std::string& name,
                  VkShaderStageFlagBits stage,
                  VkSpecializationInfo* specialization,
                  std::vector<VkShaderModule>& modules,
                  std::vector<VkPipelineShaderStageCreateInfo>& infos)
{
    std::string path = "shaders/compiled/" + name + shaderExtension(stage);
    std::vector<char> code;
    try
    {
        code = readFile(path);
    }
    catch(const std::exception&)
    {
        path = "../shaders/compiled/" + name + shaderExtension(stage);
        code = readFile(path);
    }

    VkShaderModuleCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    create.codeSize = code.size();
    create.pCode = reinterpret_cast<const uint32_t*>(code.data());

    VkShaderModule shader;
    if(hasError(vkCreateShaderModule(instance.device, &create, nullptr, &shader)))
    {
        std::string error = "Failed to create shader module: " + name;
        throw std::runtime_error(error);
    }

    VkPipelineShaderStageCreateInfo pipelineStage = {};
    pipelineStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipelineStage.stage = stage;
    pipelineStage.module = shader;
    pipelineStage.pName = "main";
    pipelineStage.pSpecializationInfo = specialization;

    modules.push_back(shader);
    infos.push_back(pipelineStage);
}

void Pipelines::create(const Instance &instance,
                       const Swapchain &swapchain,
                       const VkRenderPass &renderpass,
                       nlohmann::json &pipelineConfig,
                       uint32_t numberOfLights)
{
    createDesciptorSetLayout(instance);
    createTextureSampler(instance);

    std::vector<VkPushConstantRange> ranges(1);

    ranges[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    ranges[0].offset = push.vertexOffset();
    ranges[0].size = push.vertexSize();

    if(numberOfLights > 0)
    {
        VkPushConstantRange range = {};
        range.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
        range.offset = PushConstants::fragmentOffset();
        range.size = PushConstants::fragmentSize(numberOfLights);
        ranges.push_back(range);
    }

    VkPipelineLayoutCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    create.setLayoutCount = 1;
    create.pSetLayouts = &descriptor.layout;
    create.pushConstantRangeCount = (uint32_t)ranges.size();
    create.pPushConstantRanges = ranges.data();

    if(hasError(vkCreatePipelineLayout(instance.device, &create, nullptr, &layout)))
    {
        throw std::runtime_error("Failed to create pipeline layout");
    }

    for(auto& pipelineJson : pipelineConfig)
    {
        bool useTessellation = pipelineJson["useTessellation"];

        std::vector<VkShaderModule> shaderModules;
        std::vector<VkPipelineShaderStageCreateInfo> shaderInfos;

        VkSpecializationMapEntry specializationEntry = {};
        specializationEntry.constantID = 0;
        specializationEntry.size = sizeof(numberOfLights);
        specializationEntry.offset = 0;

        VkSpecializationInfo specialization = {};
        specialization.dataSize = sizeof(numberOfLights);
        specialization.mapEntryCount = 1;
        specialization.pMapEntries = &specializationEntry;
        specialization.pData = &numberOfLights;

        const std::string& vert = pipelineJson["vertexShader"].get<std::string>();
        const std::string& frag = pipelineJson["fragmentShader"].get<std::string>();

        createShader(instance, vert, VK_SHADER_STAGE_VERTEX_BIT, nullptr, shaderModules, shaderInfos);
        createShader(instance, frag, VK_SHADER_STAGE_FRAGMENT_BIT, &specialization, shaderModules, shaderInfos);

        if(useTessellation)
        {
            const std::string& tesc = pipelineJson["tessellationControlShader"].get<std::string>();
            const std::string& tese = pipelineJson["tessellationEvaluationShader"].get<std::string>();

            createShader(instance, tesc, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT, nullptr, shaderModules, shaderInfos);
            createShader(instance, tese, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT, nullptr, shaderModules, shaderInfos);
        }

        auto binding = Vertex::binding();
        auto attributes = Vertex::attributes();
        VkPipelineVertexInputStateCreateInfo input = {};
        input.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        input.vertexBindingDescriptionCount = 1;
        input.pVertexBindingDescriptions = &binding;
        input.vertexAttributeDescriptionCount = (uint32_t)attributes.size();
        input.pVertexAttributeDescriptions = attributes.data();

        VkPipelineInputAssemblyStateCreateInfo assembly = {};
        assembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
        assembly.topology = useTessellation? VK_PRIMITIVE_TOPOLOGY_PATCH_LIST : VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        assembly.primitiveRestartEnable = VK_FALSE;

        VkViewport viewport = {};
        viewport.x = 0.0f;
        viewport.y = 0.0f;
        viewport.width = (float)swapchain.extent.width;
        viewport.height = (float)swapchain.extent.height;
        viewport.minDepth = 0.0f;
        viewport.maxDepth = 1.0f;

        VkRect2D scissor = {};
        scissor.offset = {0, 0};
        scissor.extent = swapchain.extent;

        VkPipelineViewportStateCreateInfo viewportState = {};
        viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
        viewportState.viewportCount = 1;
        viewportState.pViewports = &viewport;
        viewportState.scissorCount = 1;
        viewportState.pScissors = &scissor;

        VkPipelineRasterizationStateCreateInfo rasterizer = {};
        rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
        rasterizer.depthClampEnable = VK_FALSE;
        rasterizer.rasterizerDiscardEnable = VK_FALSE;
        rasterizer.polygonMode = pipelineJson["wireframe"].get<bool>()? VK_POLYGON_MODE_LINE : VK_POLYGON_MODE_FILL;
        rasterizer.lineWidth = 1.0f;
        rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
        rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
        rasterizer.depthBiasEnable = VK_FALSE;
        rasterizer.depthBiasConstantFactor = 0.0f;
        rasterizer.depthBiasClamp = 0.0f;
        rasterizer.depthBiasSlopeFactor = 0.0f;

        VkPipelineMultisampleStateCreateInfo multisample = {};
        multisample.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
        multisample.sampleShadingEnable = VK_FALSE;
        multisample.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
        multisample.minSampleShading = 1.0f;
        multisample.pSampleMask = nullptr;
        multisample.alphaToCoverageEnable = VK_FALSE;
        multisample.alphaToOneEnable = VK_FALSE;

        VkPipelineColorBlendAttachmentState blend = {};
        blend.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
        blend.blendEnable = VK_FALSE;
        blend.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
        blend.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO;
        blend.colorBlendOp = VK_BLEND_OP_ADD;
        blend.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
        blend.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
        blend.alphaBlendOp = VK_BLEND_OP_ADD;

        VkPipelineColorBlendStateCreateInfo blendState = {};
        blendState.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
        blendState.logicOpEnable = VK_FALSE;
        blendState.logicOp = VK_LOGIC_OP_COPY;
        blendState.attachmentCount = 1;
        blendState.pAttachments = &blend;
        blendState.blendConstants[0] = 0.0f;
        blendState.blendConstants[1] = 0.0f;
        blendState.blendConstants[2] = 0.0f;
        blendState.blendConstants[3] = 0.0f;

        VkPipelineDepthStencilStateCreateInfo depthStencil = {};
        depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
        depthStencil.depthTestEnable = VK_TRUE;
        depthStencil.depthWriteEnable = VK_TRUE;
        depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
        depthStencil.depthBoundsTestEnable = VK_FALSE;
        depthStencil.minDepthBounds = 0.0f;
        depthStencil.maxDepthBounds = 1.0f;
        depthStencil.stencilTestEnable = VK_FALSE;
        depthStencil.front = {};
        depthStencil.back = {};

        VkGraphicsPipelineCreateInfo pipelineCreateInfo = {};
        pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        pipelineCreateInfo.stageCount = (uint32_t)shaderInfos.size();
        pipelineCreateInfo.pStages = shaderInfos.data();
        pipelineCreateInfo.pVertexInputState = &input;
        pipelineCreateInfo.pInputAssemblyState = &assembly;
        pipelineCreateInfo.pViewportState = &viewportState;
        pipelineCreateInfo.pRasterizationState = &rasterizer;
        pipelineCreateInfo.pMultisampleState = &multisample;
        pipelineCreateInfo.pDepthStencilState = nullptr;
        pipelineCreateInfo.pColorBlendState = &blendState;
        pipelineCreateInfo.pDynamicState = nullptr;
        pipelineCreateInfo.layout = layout;
        pipelineCreateInfo.renderPass = renderpass;
        pipelineCreateInfo.subpass = 0;
        pipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
        pipelineCreateInfo.basePipelineIndex = -1;
        pipelineCreateInfo.pDepthStencilState = &depthStencil;

        VkPipelineTessellationStateCreateInfo tessellation = {};
        if(useTessellation)
        {
            tessellation.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
            tessellation.patchControlPoints = pipelineJson["patchControlPoints"].get<int>();
            pipelineCreateInfo.pTessellationState = &tessellation;
        }

        VkPipeline handle;
        if(hasError(vkCreateGraphicsPipelines(instance.device, VK_NULL_HANDLE, 1, &pipelineCreateInfo, nullptr, &handle)))
        {
            throw std::runtime_error("Failed to create graphics pipeline");
        }
        handles.push_back(handle);
        for(const auto& module : shaderModules)
        {
            vkDestroyShaderModule(instance.device, module, nullptr);
        }
    }

}

const VkPipeline &Pipelines::operator[](uint32_t index) const
{
    return handles[index];
}

void Pipelines::clear(const Instance &instance)
{
    for(const auto& handle : handles)
    {
        vkDestroyPipeline(instance.device, handle, nullptr);
    }
    handles.clear();
    vkDestroyPipelineLayout(instance.device, layout, nullptr);
}

void Pipelines::createDesciptorSetLayout(const Instance &instance)
{
    if(descriptor.layout != VK_NULL_HANDLE)
    {
        return;
    }
    VkDescriptorSetLayoutBinding uboBinding = {};
    uboBinding.binding = 0;
    uboBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboBinding.descriptorCount = 1;
    uboBinding.stageFlags = VK_SHADER_STAGE_ALL;
    uboBinding.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutBinding samplerBinding = {};
    samplerBinding.binding = 1;
    samplerBinding.descriptorCount = 1;
    samplerBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerBinding.stageFlags = VK_SHADER_STAGE_ALL;
    samplerBinding.pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutBinding dynamicBinding = {};
    dynamicBinding.binding = 2;
    dynamicBinding.descriptorCount = 1;
    dynamicBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    dynamicBinding.stageFlags = VK_SHADER_STAGE_ALL;
    dynamicBinding.pImmutableSamplers = nullptr;

    std::vector<VkDescriptorSetLayoutBinding> bindings = {uboBinding, samplerBinding, dynamicBinding};

    VkDescriptorSetLayoutCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    create.bindingCount = (uint32_t)bindings.size();
    create.pBindings = bindings.data();

    if(hasError(vkCreateDescriptorSetLayout(instance.device, &create, nullptr, &descriptor.layout)))
    {
        throw std::runtime_error("Failed to create descriptor set layout");
    }
}

void Pipelines::createDescriptors(const Instance &instance, std::unordered_map<std::string, Texture> &textures, uint32_t objectCount)
{
    if(descriptor.pool != VK_NULL_HANDLE)
    {
        return;
    }

    if(textures.empty())
    {
        throw std::runtime_error("Cannot handle 0 textures at the moment");
    }

    std::array<VkDescriptorPoolSize, 3> sizes = {};
    sizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    sizes[0].descriptorCount = (uint32_t )textures.size();
    sizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    sizes[1].descriptorCount = (uint32_t )textures.size();
    sizes[2].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    sizes[2].descriptorCount = (uint32_t )textures.size();

    VkDescriptorPoolCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    create.poolSizeCount = (uint32_t)sizes.size();
    create.pPoolSizes = sizes.data();
    create.maxSets = (uint32_t )textures.size();

    if(hasError(vkCreateDescriptorPool(instance.device, &create, nullptr, &descriptor.pool)))
    {
        throw std::runtime_error("Failed to create descriptor pool");
    }

    createUniforms(instance, objectCount);

    for(auto& texture : textures)
    {
        texture.second.createDescriptorSet(instance, *this);
    }
}

void Pipelines::createTextureSampler(const Instance &instance)
{
    if(sampler != VK_NULL_HANDLE)
    {
        return;
    }

    VkSamplerCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    create.magFilter = VK_FILTER_LINEAR;
    create.minFilter = VK_FILTER_LINEAR;
    create.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    create.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    create.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    create.anisotropyEnable = VK_TRUE;
    create.maxAnisotropy = 16;
    create.borderColor = VK_BORDER_COLOR_INT_OPAQUE_WHITE;
    create.unnormalizedCoordinates = VK_FALSE;
    create.compareEnable = VK_FALSE;
    create.compareOp = VK_COMPARE_OP_ALWAYS;
    create.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    create.mipLodBias = 0.0f;
    create.minLod = 0.0f;
    create.maxLod = 0.0f;

    if(hasError(vkCreateSampler(instance.device, &create, nullptr, &sampler)))
    {
        throw std::runtime_error("Failed to create texture sampler");
    }
}

void Pipelines::createUniforms(const Instance &instance, uint32_t objectCount)
{
    VkDeviceSize size = sizeof(UniformBufferGlobal);
    createBuffer(instance,
                 size,
                 VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                 uniform.global,
                 uniform.globalMemory);

    size_t alignment = instance.properties.limits.minUniformBufferOffsetAlignment;
    size_t structSize = sizeof(UniformBufferLocal);
    uint32_t dynamicAlignment = (uint32_t)((structSize / alignment) * alignment + ((structSize % alignment) > 0 ? alignment : 0));
    size = objectCount * dynamicAlignment;
    uniform.localAlignment = dynamicAlignment;

    createBuffer(instance,
                 size,
                 VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT,
                 uniform.local,
                 uniform.localMemory);
}

void Pipelines::clean(const Instance &instance)
{
    vkDestroySampler(instance.device, sampler, nullptr);
    if(descriptor.pool != VK_NULL_HANDLE) vkDestroyDescriptorPool(instance.device, descriptor.pool, nullptr);

    vkDestroyDescriptorSetLayout(instance.device, descriptor.layout, nullptr);

    if(uniform.global != VK_NULL_HANDLE) vkDestroyBuffer(instance.device, uniform.global, nullptr);
    if(uniform.local != VK_NULL_HANDLE) vkDestroyBuffer(instance.device, uniform.local, nullptr);
    if(uniform.globalMemory) vkFreeMemory(instance.device, uniform.globalMemory, nullptr);
    if(uniform.localMemory) vkFreeMemory(instance.device, uniform.localMemory, nullptr);

}
