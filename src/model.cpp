#include <unordered_map>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>
#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>

#include "model.h"

void Model::load(ArrayObject& array,  const std::string &filename)
{
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;
    std::string error;

    std::string path = "models/" + filename;

    if(!tinyobj::LoadObj(&attrib, &shapes, &materials, &error, path.c_str()))
    {
        path = "../models/" + filename;
        if(!tinyobj::LoadObj(&attrib, &shapes, &materials, &error, path.c_str()))
        {
            throw std::runtime_error(error);
        }
    }

    std::unordered_map<Vertex, uint32_t, Vertex::Hasher> unique;

    offset = (uint32_t)array.indexData.size();
    for(const auto& shape : shapes)
    {
        for(const auto& index : shape.mesh.indices)
        {
            Vertex vertex;

            vertex.position = {
                    attrib.vertices[3 * index.vertex_index + 0],
                    attrib.vertices[3 * index.vertex_index + 1],
                    attrib.vertices[3 * index.vertex_index + 2]
            };


            vertex.normal = {
                    attrib.normals[3 * index.normal_index + 0],
                    attrib.normals[3 * index.normal_index + 1],
                    attrib.normals[3 * index.normal_index + 2],
            };

            vertex.color = {1.0f, 1.0f, 1.0f};

            if(attrib.texcoords.empty())
            {
                vertex.texCoord = {0, 0};
            }
            else
            {
                vertex.texCoord = {
                        attrib.texcoords[2 * index.texcoord_index + 0],
                        1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
                };
            }

            if(unique.count(vertex) == 0)
            {
                unique[vertex] = (uint32_t)array.vertexData.size();
                array.vertexData.push_back(vertex);
            }
            array.indexData.push_back(unique[vertex]);
        }
    }
    size = (uint32_t)array.indexData.size() - offset;
}

bool Vertex::operator==(const Vertex &vertex) const
{
    return position == vertex.position &&
            color == vertex.color &&
            normal == vertex.normal &&
            texCoord == vertex.texCoord;
}

VkVertexInputBindingDescription Vertex::binding()
{
    VkVertexInputBindingDescription bind = {};
    bind.binding = 0;
    bind.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    bind.stride = sizeof(Vertex);
    return bind;
}

std::array<VkVertexInputAttributeDescription, 4> Vertex::attributes()
{
    std::array<VkVertexInputAttributeDescription, 4> attr = {};
    attr[0].binding = 0;
    attr[0].location = 0;
    attr[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    attr[0].offset = (uint32_t)offsetof(Vertex, position);

    attr[1].binding = 0;
    attr[1].location = 1;
    attr[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    attr[1].offset = (uint32_t)offsetof(Vertex, normal);

    attr[2].binding = 0;
    attr[2].location = 2;
    attr[2].format = VK_FORMAT_R32G32B32_SFLOAT;
    attr[2].offset = (uint32_t)offsetof(Vertex, color);

    attr[3].binding = 0;
    attr[3].location = 3;
    attr[3].format = VK_FORMAT_R32G32_SFLOAT;
    attr[3].offset = (uint32_t)offsetof(Vertex, texCoord);

    return attr;
}

std::size_t Vertex::Hasher::operator()(const Vertex &vertex) const
{
    size_t h = std::hash<glm::vec3>()(vertex.position) << 1;
    h ^= std::hash<glm::vec3>()(vertex.normal);
    h ^= std::hash<glm::vec3>()(vertex.color) >> 1;
    h ^= std::hash<glm::vec2>()(vertex.texCoord);
    return h;
}
