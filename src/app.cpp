#include "app.h"

#include <fstream>

#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_RADIANS

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/euler_angles.hpp>

#include "vkutil.h"
#include "logging.h"
#include "enums.h"
#include "extensionloader.h"
#include "util.h"

using uint = unsigned int;

static VKAPI_ATTR VkBool32 VKAPI_CALL logValidationError(
        VkDebugReportFlagsEXT flags,
        VkDebugReportObjectTypeEXT objectType,
        uint64_t object,
        size_t location,
        int32_t code,
        const char* layerPrefix,
        const char* message,
        void* userData
)
{
    std::cerr << "[VALIDATION]: " << flags << " - " << objectType << " - " << object << " - " << location << std::endl;
    std::cerr << "              " << code << " - " << layerPrefix << " - " << userData << std::endl;
    std::cerr << "     Message: " << message << std::endl << std::endl;
    return VK_FALSE;
}


VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeatureFlags features, VkPhysicalDevice physicalDevice)
{
    for(auto format : candidates)
    {
        VkFormatProperties prop = {};
        vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &prop);

        if(tiling == VK_IMAGE_TILING_LINEAR && (prop.linearTilingFeatures & features) == features)
        {
            return format;
        }
        if(tiling == VK_IMAGE_TILING_OPTIMAL && (prop.optimalTilingFeatures & features) == features)
        {
            return format;
        }
    }
    throw std::runtime_error("Failed to find supported format");
}

VkFormat findDepthFormat(VkPhysicalDevice physicalDevice)
{
    return findSupportedFormat(
            {VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT},
            VK_IMAGE_TILING_OPTIMAL,
            VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT,
            physicalDevice
    );
}

App::App(uint32_t width, uint32_t height, bool enableValidation, json scene)
    : camera(scene["camera"]), enableValidation(enableValidation), width(width), height(height)
{
    this->scene.useSkybox = scene["useSkybox"].get<bool>();
    this->scene.forceFifo = scene["surface"]["forceFifo"].get<bool>();
    this->scene.config = std::move(scene);
}

App::~App()
{
    cleanSwapChain();


    for(auto& texture : scene.textures)
    {
        texture.second.clean(instance);
    }

    pipelines.clean(instance);
    array.clean(instance);
    if(renderFinished != VK_NULL_HANDLE) vkDestroySemaphore(instance.device, renderFinished, nullptr);
    if(imageAvailable != VK_NULL_HANDLE) vkDestroySemaphore(instance.device, imageAvailable, nullptr);
    vkDestroyCommandPool(instance.device, instance.pool, nullptr);

    vDestroyDebugReportCallback(instance.get(), debugCallback, nullptr);
    instance.clean();

    glfwDestroyWindow(window);
    glfwTerminate();
}

void App::init()
{
    initWindow();
    initVulkan();
    startTime = std::chrono::high_resolution_clock::now();
    totalStartTime = std::chrono::high_resolution_clock::now();

}

bool App::shouldRun() const
{
    return !glfwWindowShouldClose(window);
}

float App::loop()
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }
    auto endTime = std::chrono::high_resolution_clock::now();
    deltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(endTime - startTime).count();
    totalTime = std::chrono::duration_cast<std::chrono::duration<float>>(endTime - totalStartTime).count();
    startTime = endTime;
    camera.update(window, deltaTime);
    updateUniformBuffers();
    updatePushConstants();
    data.pushConstants.totalTime += 0.1f * deltaTime;
    drawFrame();

    return deltaTime;
}

void App::shutdown()
{
    vkDeviceWaitIdle(instance.device);
}

void App::initWindow()
{
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

    window = glfwCreateWindow(width, height, "Vulkan", nullptr, nullptr);
    Lua::setWindow(window);

    glfwSetWindowUserPointer(window, this);
    glfwSetWindowSizeCallback(window, App::onWindowResized);
    glfwSetWindowFocusCallback(window, App::onFocusChanged);
    glfwSetCursorPos(window, width / 2, height / 2);
}

void App::initVulkan()
{
    if(enableValidation)
    {
        logInstanceExtensions();
        logInstanceLayers();
    }
    createInstance();
    if(enableValidation)
    {
        createDebugCallback();
    }
    createSurface();
    findPhysicalDevice();
    createLogicalDevice();
    updatePushConstants();
    swapchain.create(instance, width, height, scene.forceFifo);
    createRenderPass();
    pipelines.create(instance, swapchain, renderPass, scene.config["pipelines"], (uint32_t)scene.config["lights"].size());
    createCommandPool();
    createDepthResources();
    createFramebuffers();
    loadObjects();
    array.create(instance);
    pipelines.createDescriptors(instance, scene.textures, (uint32_t)scene.objects.size());
    data.uniformLocal.resize((uint32_t)scene.objects.size());
    createDrawCommandBuffers();
    createPushCommandBuffers();
    createSemaphores();
}

void App::createInstance()
{
    auto layers = validationLayers;

    VkApplicationInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    info.pApplicationName = "Vulkan";
    info.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
    info.pEngineName = "None";
    info.engineVersion = VK_MAKE_VERSION(0, 0, 0);
    info.apiVersion = VK_API_VERSION_1_0;

    VkInstanceCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    create.pApplicationInfo = &info;

    unsigned int glfwExtensionCount = 0;
    const char **glfwExtensions;

    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
    std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

    if(enableValidation)
    {
        extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
        std::clog << "Required extensions: " << std::endl;
        for(auto ext : extensions)
        {
            std::clog << " - " << ext << std::endl;
        }
    }

    create.enabledExtensionCount = static_cast<uint32_t >(extensions.size());
    create.ppEnabledExtensionNames = extensions.data();
    create.enabledLayerCount = 0;

    if (enableValidation) {
        std::clog << "Enabling validation" << std::endl;
        create.enabledLayerCount = static_cast<uint32_t>(layers.size());
        create.ppEnabledLayerNames = layers.data();
    }

    if (hasError(vkCreateInstance(&create, nullptr, instance.set()))) {
        std::cerr << "Failed to create instance" << std::endl;
        throw std::runtime_error("Failed to create instance");
    }
}

void App::createDebugCallback()
{
    VkDebugReportCallbackCreateInfoEXT create = {};
    create.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    create.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
    create.pfnCallback = logValidationError;

    if(hasError(vCreateDebugReportCallback(instance.get(), &create, nullptr, &debugCallback)))
    {
        std::cerr << "Failed to create debug callback" << std::endl;
        throw std::runtime_error("Failed to create debug callback");
    }
}

void App::findPhysicalDevice()
{
    uint32_t size = 0;
    vkEnumeratePhysicalDevices(instance.get(), &size, nullptr);
    if(size == 0)
    {
        throw std::runtime_error("Failed to find any physical device");
    }

    std::vector<VkPhysicalDevice> values(size);
    vkEnumeratePhysicalDevices(instance.get(), &size, values.data());
    std::multimap<float, VkPhysicalDevice> scores;

    if(enableValidation)
    {
        std::clog << "Found " << size << " physical devices" << std::endl;
    }
    for(const auto& device : values)
    {
        scores.insert(std::make_pair(getDeviceScore(device), device));
    }

    if(scores.rbegin()->first == 0)
    {
        throw std::runtime_error("Failed to find suitable physical device");
    }
    instance.physicalDevice = scores.rbegin()->second;
    vkGetPhysicalDeviceProperties(instance.physicalDevice, &instance.properties);
}

float App::getDeviceScore(VkPhysicalDevice device)
{
    VkPhysicalDeviceProperties prop = {};
    vkGetPhysicalDeviceProperties(device, &prop);
    VkPhysicalDeviceFeatures feat = {};
    vkGetPhysicalDeviceFeatures(device, &feat);

    if(enableValidation)
    {
        std::clog << " - " << device << std::endl;

        std::clog << "   - id     " << prop.deviceID << std::endl;
        std::clog << "   - name   " << prop.deviceName << std::endl;
        std::clog << "   - type   " << physicalDeviceType(prop.deviceType) << std::endl;
        std::clog << "   - api    " << unpackVersion(prop.apiVersion) << std::endl;
        std::clog << "   - driver " << unpackVersion(prop.driverVersion) << std::endl;
        std::clog << "   - vendor " << prop.vendorID << std::endl;

        std::clog << "   - pipeid " << uuidToHex(prop.pipelineCacheUUID, sizeof(prop.pipelineCacheUUID)) << std::endl;
    }

    float score;

    switch(prop.deviceType)
    {
    case VK_PHYSICAL_DEVICE_TYPE_CPU:
        score = 0;
        break;
    case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU:
        score = 1000;
        break;
    default:
        score = 1;
    }

    uint32_t pushSizeNeeded = PushConstants::totalSize((uint32_t)scene.config["lights"].size());
    if(prop.limits.maxPushConstantsSize < pushSizeNeeded)
    {
        if(enableValidation)
        {
            std::clog << "   - problem: max push constant size too small" << std::endl;
            std::clog << "              needed " << pushSizeNeeded << " got " << prop.limits.maxPushConstantsSize << std::endl;
        }
        score = 0;
    }

    if(!feat.geometryShader)
    {
        if(enableValidation)
        {
            std::clog << "   - problem: missing geometry shader support" << std::endl;
        }
        score = 0;
    }

    auto families = QueueFamilies::find(device, instance.surface);
    if(!families.isComplete())
    {
        if(enableValidation)
        {
            std::clog << "   - problem: missing suitable queue family" << std::endl;
        }
        score = 0;
    }

    uint32_t size = 0;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &size, nullptr);
    std::vector<VkExtensionProperties> values(size);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &size, values.data());

    std::set<std::string> missing(deviceExtensions.begin(), deviceExtensions.end());
    for(const auto& ext : values)
    {
        missing.erase(ext.extensionName);
    }
    if(!missing.empty())
    {
        score = 0;
        if(enableValidation)
        {
            std::clog << "   - problem: missing device extensions:" << std::endl;
            for(const auto& ext : missing)
            {
                std::clog << "     - " << ext << std::endl;
            }
        }
    }
    else
    {
        size = 0;
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, instance.surface, &size, nullptr);
        std::vector<VkSurfaceFormatKHR> formats(size);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, instance.surface, &size, formats.data());
        size = 0;
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, instance.surface, &size, nullptr);
        std::vector<VkPresentModeKHR> modes(size);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, instance.surface, &size, modes.data());

        if(formats.empty())
        {
            score = 0;
            if(enableValidation)
            {
                std::clog << "   - problem: missing surface formats" << std::endl;
            }

        }
        if(modes.empty())
        {
            score = 0;
            if(enableValidation)
            {
                std::clog << "   - problem: missing present modes" << std::endl;
            }
        }
    }

    if(!feat.samplerAnisotropy)
    {
        score = 0;
    }

    uint32_t major = VK_VERSION_MAJOR(VK_API_VERSION_1_0);
    uint32_t minor = VK_VERSION_MINOR(VK_API_VERSION_1_0);
    uint32_t version = VK_MAKE_VERSION(major, minor, VK_HEADER_VERSION);

    if(prop.apiVersion >= version)
    {
        score *= 1.1f;
    }
    if(enableValidation)
    {
        std::clog << "   - score  " << score << std::endl;
    }
    return score;
}

void App::createLogicalDevice()
{
    QueueFamilies families = QueueFamilies::find(instance);

    std::vector<VkDeviceQueueCreateInfo> createQueues;
    float priority = 1.0f;

    for(int family : families.uniqueFamilies())
    {
        VkDeviceQueueCreateInfo create = {};
        create.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        create.queueFamilyIndex = (uint32_t)family;
        create.queueCount = 1;
        create.pQueuePriorities = &priority;
        createQueues.push_back(create);
    }

    VkPhysicalDeviceFeatures features = {};
    features.samplerAnisotropy = VK_TRUE;
    for(const auto& pipeline : scene.config["pipelines"])
    {
        if(pipeline["useTessellation"].get<bool>())
        {
            features.tessellationShader = VK_TRUE;
        }
        if(pipeline["wireframe"].get<bool>())
        {
            features.fillModeNonSolid = VK_TRUE;
        }
    }

    VkDeviceCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    create.queueCreateInfoCount = (uint32_t)createQueues.size();
    create.pQueueCreateInfos = createQueues.data();
    create.enabledExtensionCount = (uint32_t)deviceExtensions.size();
    create.ppEnabledExtensionNames = deviceExtensions.data();
    create.pEnabledFeatures = &features;

    if(enableValidation)
    {
        create.enabledLayerCount = (uint32_t)validationLayers.size();
        create.ppEnabledLayerNames = validationLayers.data();
    }
    else
    {
        create.enabledLayerCount = 0;
    }

    if(hasError(vkCreateDevice(instance.physicalDevice, &create, nullptr, &instance.device)))
    {
        throw std::runtime_error("Failed to create logical device");
    }

    vkGetDeviceQueue(instance.device, (uint32_t)families.graphics, 0, &instance.queues.graphics);
    vkGetDeviceQueue(instance.device, (uint32_t)families.presentation, 0, &instance.queues.presentation);
}

void App::createSurface()
{
    if(hasError(glfwCreateWindowSurface(instance.get(), window, nullptr, &instance.surface)))
    {
        throw std::runtime_error("Failed to create window surface");
    }
}

void App::createRenderPass()
{
    VkAttachmentDescription attachment = {};
    attachment.format = swapchain.format;
    attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentReference attachmentRef = {};
    attachmentRef.attachment = 0;
    attachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription depth = {};
    depth.format = findDepthFormat(instance.physicalDevice);
    depth.samples = VK_SAMPLE_COUNT_1_BIT;
    depth.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depth.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depth.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depth.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depth.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depth.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depthRef = {};
    depthRef.attachment = 1;
    depthRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkSubpassDescription subPass = {};
    subPass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subPass.colorAttachmentCount = 1;
    subPass.pColorAttachments = &attachmentRef;
    subPass.pDepthStencilAttachment = &depthRef;

    std::vector<VkAttachmentDescription> attachments = {attachment, depth};
    VkRenderPassCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    create.attachmentCount = (uint32_t)attachments.size();
    create.pAttachments = attachments.data();
    create.subpassCount = 1;
    create.pSubpasses = &subPass;

    VkSubpassDependency dependency = {};
    dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependency.dstSubpass = 0;
    dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.srcAccessMask = 0;
    dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

    create.dependencyCount = 1;
    create.pDependencies = &dependency;

    if(hasError(vkCreateRenderPass(instance.device, &create, nullptr, &renderPass)))
    {
        throw std::runtime_error("Failed to create render pass");
    }
}

void App::createFramebuffers()
{
    framebuffers.resize(swapchain.views.size());
    for(size_t i = 0; i < framebuffers.size(); ++i)
    {
        std::vector<VkImageView> attachments = {swapchain.views[i], depthView};
        VkFramebufferCreateInfo create = {};
        create.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        create.renderPass = renderPass;
        create.attachmentCount = (uint32_t)attachments.size();
        create.pAttachments = attachments.data();
        create.width = swapchain.extent.width;
        create.height = swapchain.extent.height;
        create.layers = 1;

        if(hasError(vkCreateFramebuffer(instance.device, &create, nullptr, &framebuffers[i])))
        {
            throw std::runtime_error("Failed to create framebuffers");
        }
    }
}

void App::createCommandPool()
{
    QueueFamilies families = QueueFamilies::find(instance);

    VkCommandPoolCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    create.queueFamilyIndex = (uint32_t)families.graphics;
    create.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    if(hasError(vkCreateCommandPool(instance.device, &create, nullptr, &instance.pool)))
    {
        throw std::runtime_error("Failed to create command pool");
    }
}

void App::createDrawCommandBuffers()
{
    commandBuffers.draw.resize(framebuffers.size());

    VkCommandBufferAllocateInfo allocate = {};
    allocate.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocate.commandPool = instance.pool;
    allocate.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocate.commandBufferCount = (uint32_t)commandBuffers.draw.size();

    if(hasError(vkAllocateCommandBuffers(instance.device, &allocate, commandBuffers.draw.data())))
    {
        throw std::runtime_error("Failed to allocate draw command buffers");
    }

    recordDrawCommands();
}

void App::recordDrawCommands()
{

    for(size_t i = 0; i < commandBuffers.draw.size(); ++i)
    {
        VkCommandBufferBeginInfo begin = {};
        begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        begin.pInheritanceInfo = nullptr;

        vkBeginCommandBuffer(commandBuffers.draw[i], &begin);

        VkRenderPassBeginInfo render = {};
        render.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        render.renderPass = renderPass;
        render.framebuffer = framebuffers[i];
        render.renderArea.offset = {0, 0};
        render.renderArea.extent = swapchain.extent;

        std::array<VkClearValue, 2> clear = {};
        clear[0].color = {0.1f, 0.1f, 0.1f, 1.0f};
        clear[1].depthStencil = {1.0f, 0};

        render.clearValueCount = (uint32_t)clear.size();
        render.pClearValues = clear.data();

        vkCmdBeginRenderPass(commandBuffers.draw[i], &render, VK_SUBPASS_CONTENTS_INLINE);



        VkDeviceSize offsets[] = {0};
        vkCmdBindVertexBuffers(commandBuffers.draw[i], 0, 1, &array.vertexBuffer, offsets);
        vkCmdBindIndexBuffer(commandBuffers.draw[i], array.indexBuffer, 0, VK_INDEX_TYPE_UINT32);

        if(!data.pushConstants.lights.empty())
        {
            vkCmdPushConstants(commandBuffers.draw[i], pipelines.layout, VK_SHADER_STAGE_FRAGMENT_BIT, PushConstants::fragmentOffset(), data.pushConstants.fragmentSize(), data.pushConstants.lights.data());
        }

        uint32_t dynamicOffset = 0;
        for (const auto &object : scene.objects)
        {
            vkCmdBindPipeline(commandBuffers.draw[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines[object.pipelineIndex]);

            vkCmdBindDescriptorSets(commandBuffers.draw[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelines.layout, 0, 1,
                                    &object.texture.descriptorSet, 1, &dynamicOffset);

            dynamicOffset += pipelines.uniform.localAlignment;
            vkCmdDrawIndexed(commandBuffers.draw[i], object.model.size, object.instanceCount, object.model.offset, 0, object.firstInstance);
        }



        vkCmdEndRenderPass(commandBuffers.draw[i]);

        if(hasError(vkEndCommandBuffer(commandBuffers.draw[i])))
        {
            throw std::runtime_error("Failed to record command buffers");
        }
    }
}

void App::createSemaphores()
{
    VkSemaphoreCreateInfo create = {};
    create.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    if(hasError(vkCreateSemaphore(instance.device, &create, nullptr, &imageAvailable)) ||
       hasError(vkCreateSemaphore(instance.device, &create, nullptr, &renderFinished)))
    {
        throw std::runtime_error("Failed to create semaphores");
    }
}

void App::drawFrame()
{
    vkQueueWaitIdle(instance.queues.presentation);

    uint32_t image;
    VkResult result = vkAcquireNextImageKHR(instance.device, swapchain.get(), std::numeric_limits<uint64_t>::max(), imageAvailable, VK_NULL_HANDLE, &image);

    if(result == VK_ERROR_OUT_OF_DATE_KHR)
    {
        recreateSwapChain();
        return;
    }
    else if(result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
    {
        throw std::runtime_error("Failed to acquire swap chain image");
    }

    recordPushCommands();
    submitPushCommands();

    VkSubmitInfo submit = {};
    submit.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    VkPipelineStageFlags wait[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};
    submit.waitSemaphoreCount = 1;
    submit.pWaitSemaphores = &imageAvailable;
    submit.pWaitDstStageMask = wait;
    submit.commandBufferCount = 1;
    submit.pCommandBuffers = &commandBuffers.draw[image];
    submit.signalSemaphoreCount = 1;
    submit.pSignalSemaphores = &renderFinished;

    if(hasError(vkQueueSubmit(instance.queues.graphics, 1, &submit, VK_NULL_HANDLE)))
    {
        throw std::runtime_error("Failed to submit draw command buffer");
    }

    VkPresentInfoKHR present = {};
    present.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present.waitSemaphoreCount = 1;
    present.pWaitSemaphores = &renderFinished;
    present.swapchainCount = 1;
    present.pSwapchains = &swapchain.get();
    present.pImageIndices = &image;
    present.pResults = nullptr;

    result = vkQueuePresentKHR(instance.queues.presentation, &present);

    if(result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
    {
        recreateSwapChain();
    }
    else if(hasError(result))
    {
        throw std::runtime_error("Failed to present swap chain image");
    }
}

void App::recreateSwapChain()
{
    vkDeviceWaitIdle(instance.device);

    cleanSwapChain();

    swapchain.create(instance, width, height, scene.forceFifo);
    createRenderPass();
    pipelines.create(instance, swapchain, renderPass, scene.config["pipelines"], (uint32_t)scene.config["lights"].size());
    createDepthResources();
    createFramebuffers();
    createDrawCommandBuffers();
}

void App::cleanSwapChain()
{
    vkDestroyImageView(instance.device, depthView, nullptr);
    vkDestroyImage(instance.device, depthImage, nullptr);
    vkFreeMemory(instance.device, depthMemory, nullptr);

    for (auto& framebuffer : framebuffers)
    {
        vkDestroyFramebuffer(instance.device, framebuffer, nullptr);
    }

    vkFreeCommandBuffers(instance.device, instance.pool, (uint32_t)commandBuffers.draw.size(), commandBuffers.draw.data());
    pipelines.clear(instance);
    vkDestroyRenderPass(instance.device, renderPass, nullptr);

    swapchain.clean(instance);
}

void App::onWindowResized(GLFWwindow *window, int width, int height)
{
    if(width == 0 || height == 0)
    {
        return;
    }

    auto app = reinterpret_cast<App*>(glfwGetWindowUserPointer(window));
    app->width = (uint32_t)width;
    app->height = (uint32_t)height;
    app->recreateSwapChain();
}

void App::updateUniformBuffers()
{
    UniformBufferGlobal ubo = {};

    ubo.view = camera.view();
    ubo.projection = camera.projection();
    ubo.camera = camera.position;

    void* mapped;
    vkMapMemory(instance.device, pipelines.uniform.globalMemory, 0, sizeof(ubo), 0, &mapped);
        memcpy(mapped, &ubo, sizeof(ubo));
    vkUnmapMemory(instance.device, pipelines.uniform.globalMemory);

    for(uint32_t i = 0; i < scene.objects.size(); ++i)
    {
        auto& object = scene.objects[i];

        if(!object.luaKey.empty())
        {
            lua.interpreter.update(object.luaKey, totalTime, deltaTime, object, camera);
        }

        auto transform = glm::mat4(1);
        transform = glm::translate(transform, object.position);
        transform *= glm::eulerAngleXYZ(object.rotation.x, object.rotation.y, object.rotation.z);
        transform = glm::scale(transform, object.scale);

        data.uniformLocal[i].model = transform;
        data.uniformLocal[i].update();
        data.uniformLocal[i].color = object.color;

        vkMapMemory(instance.device, pipelines.uniform.localMemory, pipelines.uniform.localAlignment * i, pipelines.uniform.localAlignment, 0, &mapped);
            memcpy(mapped, &this->data.uniformLocal[i], sizeof(UniformBufferLocal));
        vkUnmapMemory(instance.device, pipelines.uniform.localMemory);
    }
}

void App::createDepthResources()
{
    VkFormat depthFormat = findDepthFormat(instance.physicalDevice);
    createImage(instance,
                swapchain.extent.width,
                swapchain.extent.height,
                0,
                depthFormat,
                VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                depthImage,
                depthMemory);
    depthView = createImageView(instance,
                                depthImage,
                                VK_IMAGE_VIEW_TYPE_2D,
                                depthFormat,
                                VK_IMAGE_ASPECT_DEPTH_BIT);
    transitionImageLayout(instance,
                          depthImage,
                          depthFormat,
                          VK_IMAGE_LAYOUT_UNDEFINED,
                          VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                          false);
}

void App::onFocusChanged(GLFWwindow *window, int focused)
{
    auto app = reinterpret_cast<App*>(glfwGetWindowUserPointer(window));
    app->camera.disable(!focused);
}

void App::updatePushConstants()
{
    data.pushConstants.lights.clear();
    for(const auto& light : scene.config["lights"])
    {
        data.pushConstants.lights.push_back(jsonToVec3(light["position"]));
    }

    data.pushConstants.totalTime = totalTime;
}

void App::recordPushCommands()
{
    size_t id = commandBuffers.currentPush;
    const auto& buffer = commandBuffers.push[id];

    VkCommandBufferBeginInfo begin = {};
    begin.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
    begin.pInheritanceInfo = nullptr;

    vkBeginCommandBuffer(buffer, &begin);
    vkCmdPushConstants(buffer, pipelines.layout, VK_SHADER_STAGE_VERTEX_BIT, PushConstants::vertexOffset(), PushConstants::vertexSize(), &data.pushConstants.totalTime);

    if(hasError(vkEndCommandBuffer(buffer)))
    {
        throw std::runtime_error("Failed to record push command buffer");
    }
}

void App::submitPushCommands()
{
    size_t id = commandBuffers.currentPush;
    const auto& buffer = commandBuffers.push[id];

    VkSubmitInfo submit = {};
    submit.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit.commandBufferCount = 1;
    submit.pCommandBuffers = &buffer;

    if(hasError(vkQueueSubmit(instance.queues.graphics, 1, &submit, VK_NULL_HANDLE)))
    {
        throw std::runtime_error("Failed to submit push command queue");
    }
    commandBuffers.currentPush = (id + 1) % commandBuffers.push.size();
}

void App::createPushCommandBuffers()
{
    VkCommandBufferAllocateInfo allocate = {};
    allocate.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocate.commandPool = instance.pool;
    allocate.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocate.commandBufferCount = (uint32_t)commandBuffers.push.size();

    if(hasError(vkAllocateCommandBuffers(instance.device, &allocate, commandBuffers.push.data())))
    {
        throw std::runtime_error("Failed to allocate push command buffers");
    }
}

void App::loadObjects()
{
    for(auto& object : scene.config["objects"])
    {
        const Texture& texture = loadTexture(object["texture"].get<std::string>());
        const Model& model = loadModel(object["model"].get<std::string>());
        object["luaKey"] = loadLuaScript(object["script"].get<std::string>());

        scene.objects.emplace_back(model, texture, object);
    }

    if(scene.useSkybox)
    {
        scene.textures["__skybox__"].loadSkybox(instance, scene.config["skybox"]);
        scene.models["__skybox__"].load(array, "simple/skybox.obj");

        scene.objects.emplace_back(scene.models["__skybox__"], scene.textures["__skybox__"], scene.config["skybox"]["pipeline"].get<int>());
    }
}

const Model &App::loadModel(const std::string &filename)
{
    if(scene.models.count(filename) == 0)
    {
        scene.models[filename].load(array, filename);
    }
    return scene.models[filename];
}

const Texture &App::loadTexture(const std::string &filename)
{
    if(scene.textures.count(filename) == 0)
    {
        scene.textures[filename].load(instance, filename);
    }
    return scene.textures[filename];
}

std::string App::loadLuaScript(const std::string &script)
{
    if(script.empty())
    {
        return "";
    }

    std::string key = "VulkanObject" + std::to_string(lua.scriptCount++);
    lua.interpreter.load(script, key);

    return key;
}
