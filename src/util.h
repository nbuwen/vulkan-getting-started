#ifndef VULKANGETTINGSTARTED_UTIL_H
#define VULKANGETTINGSTARTED_UTIL_H

#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <json.hpp>

glm::vec3 jsonToVec3(nlohmann::json array);
std::string directory(const std::string&);

#endif //VULKANGETTINGSTARTED_UTIL_H
