#ifndef VULKANGETTINGSTARTED_POPUP_H
#define VULKANGETTINGSTARTED_POPUP_H

#include <string>

void popup(const std::string& title, const std::string& message);
std::string abspath(const std::string& relpath);

#endif //VULKANGETTINGSTARTED_POPUP_H
