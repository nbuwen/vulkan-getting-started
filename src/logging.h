#ifndef TRIANGLE_LOGGING_H
#define TRIANGLE_LOGGING_H

#include <vector>
#include <iostream>

#include <vulkan/vulkan.h>


void logInstanceExtensions();
void logInstanceLayers();


#endif //TRIANGLE_LOGGING_H
