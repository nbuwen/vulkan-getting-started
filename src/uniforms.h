#ifndef VULKANGETTINGSTARTED_UNIFORMS_H
#define VULKANGETTINGSTARTED_UNIFORMS_H

#include <vector>

#include <glm/glm.hpp>

struct PushConstants
{
    std::vector<glm::vec3> lights;
    float totalTime;

    static uint32_t fragmentOffset();
    static uint32_t fragmentSize(uint32_t lightCount);
    uint32_t fragmentSize() const;
    static uint32_t vertexOffset();
    static uint32_t vertexSize();
    static uint32_t totalSize(uint32_t lightCount);
};

struct UniformBufferGlobal
{
    glm::mat4 view;
    glm::mat4 projection;
    glm::vec3 camera;
};

struct UniformBufferLocal
{
    glm::mat4 model;
    glm::mat4 normal;
    glm::vec3 color;

    void update();
};

#endif //VULKANGETTINGSTARTED_UNIFORMS_H
