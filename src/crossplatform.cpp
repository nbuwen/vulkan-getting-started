#include "crossplatform.h"

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#include <windows.h>
#else
#include <iostream>
#endif

void popup(const std::string& title, const std::string& message)
{
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    MessageBox(nullptr, message.c_str(), title.c_str(), MB_OK | MB_ICONERROR);
#else
    std::string command = "/usr/bin/notify-send ";

    command = command + '"';
    command = command + title;
    command = command + '"';

    command += ' ';

    command = command + '"';
    command = command + message;
    command = command + '"';

    system(command.c_str());
    std::cerr << "[FATAL] " << title << std::endl;
    std::cerr << "        " << message << std::endl;
#endif
}

std::string abspath(const std::string& relpath)
{
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
    TCHAR buffer[MAX_PATH];
	GetFullPathNameA(relpath.c_str(), MAX_PATH, buffer, nullptr);
    return buffer;
#else
    char* buffer = realpath(relpath.c_str(), nullptr);
    std::string path(buffer);
    free(buffer);

    return path;
#endif
}


