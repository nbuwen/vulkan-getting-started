#include <glm/gtc/matrix_transform.hpp>
#include <algorithm>

#include "camera.h"
using json = nlohmann::json;

int keyAxis(GLFWwindow* window, int positiveKey, int negativeKey)
{
    return (glfwGetKey(window, positiveKey) == GLFW_PRESS) - (glfwGetKey(window, negativeKey) == GLFW_PRESS);
}

glm::mat4 Camera::view() const
{
    return glm::lookAt(position, position + _forward, _up);
}


const glm::mat4 &Camera::projection() const
{
    return _projection.matrix;
}


void Camera::update(GLFWwindow *window, float deltaTime)
{
    if(_inactive)
    {
        return;
    }

    static const float piHalf = glm::pi<float>() / 2;
    double x, y;
    int width, height;

    glfwGetCursorPos(window, &x, &y);
    glfwGetWindowSize(window, &width, &height);

    if(x < 0 || y < 0 || x > width || y > height)
    {
        return;
    }

    if(_projection.firstUpdate)
    {
        _projection.firstUpdate = false;
        setPerspective(width / (float)height);
    }

    if(glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
    {
        setPerspective(width / (float)height);
    }
    else if(glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
    {
        setOrthographic();
    }

    int changeProjectionParameter = keyAxis(window, GLFW_KEY_KP_ADD, GLFW_KEY_KP_SUBTRACT);

    if(changeProjectionParameter != 0)
    {
        if(_projection.isPerspective)
        {
            _projection.perspectiveFov += 10 * deltaTime * changeProjectionParameter;
            setPerspective(width / (float)height);
        }
        else
        {
            _projection.orthographicSize += deltaTime * changeProjectionParameter;
            setOrthographic();
        }
    }

    glfwSetCursorPos(window, width / 2, height / 2);

    _angle.x += _turnSpeed.x * (width / 2 - (float)x);
    _angle.y += _turnSpeed.y * (height / 2 - (float)y);
    _angle.y = std::min(std::max(_angle.y, -piHalf + 0.1f), piHalf - 0.1f);

    if(glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
    {
        _angle.x = 0;
        _angle.y = 0;
        position = {0, 0, -3};
    }

    _forward = {glm::cos(_angle.y) * glm::sin(_angle.x), glm::sin(_angle.y), glm::cos(_angle.y) * glm::cos(_angle.x)};
    _right = {glm::sin(_angle.x + piHalf), 0, glm::cos(_angle.x + piHalf)};

    glm::vec3 forwardMove = _forward;
    forwardMove.y = 0;
    forwardMove = glm::normalize(forwardMove);

    if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        position += forwardMove * deltaTime * _speed.x;
    }
    else if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        position += forwardMove * deltaTime * -_speed.x;
    }
    if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        position += _right * deltaTime * _speed.y;
    }
    else if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        position += _right * deltaTime * -_speed.y;
    }
    if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
    {
        position += _up * deltaTime * _speed.z;
    }
    else if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS)
    {
        position += _up * deltaTime * -_speed.z;
    }
}

void Camera::disable(bool disabled)
{
    _inactive = disabled;
}

Camera::Camera(nlohmann::json& config)
    : json(config)
{
    _speed *= config["speed"].get<float>();
    position.x = config["position"][0].get<float>();
    position.y = config["position"][1].get<float>();
    position.z = config["position"][2].get<float>();
}

void Camera::setOrthographic()
{
    float size = _projection.orthographicSize;

    _projection.matrix = glm::ortho(-size, size, -size, size, -size * 10, size * 10);
    _projection.matrix[1][1] *= -1;

    _projection.isPerspective = false;
}

void Camera::setPerspective(float ratio)
{
    _projection.matrix = glm::perspective(glm::radians(_projection.perspectiveFov), ratio, 0.1f, 10000.0f);
    _projection.matrix[1][1] *= -1;

    _projection.isPerspective = true;
}
