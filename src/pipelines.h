#ifndef VULKANGETTINGSTARTED_PIPELINES_H
#define VULKANGETTINGSTARTED_PIPELINES_H

#include <vector>

#include <vulkan/vulkan.h>
#include <unordered_map>

#include "instance.h"
#include "swapchain.h"
#include "texture.h"
#include "uniforms.h"

struct Texture;

struct Pipelines
{
    VkPipelineLayout layout = VK_NULL_HANDLE;
    std::vector<VkPipeline> handles;
    PushConstants push;
    VkSampler sampler = VK_NULL_HANDLE;

    struct {
        VkDescriptorSetLayout layout = VK_NULL_HANDLE;
        VkDescriptorPool pool = VK_NULL_HANDLE;
    } descriptor;

    struct {
        VkBuffer global;
        VkDeviceMemory globalMemory;
        VkBuffer local;
        VkDeviceMemory localMemory;
        uint32_t localAlignment;
    } uniform;

    void create(const Instance &instance, const Swapchain &swapchain, const VkRenderPass &renderpass,
                    nlohmann::json &pipelineConfig, uint32_t numberOfLights);
    void createDescriptors(const Instance &instance, std::unordered_map<std::string, Texture> &textures, uint32_t objectCount);
    void clear(const Instance& instance);
    void clean(const Instance& instance);
    const VkPipeline& operator[](uint32_t index) const;

private:
    void createDesciptorSetLayout(const Instance &instance);
    void createTextureSampler(const Instance& instance);
    void createUniforms(const Instance &instance, uint32_t objectCount);
};


#endif //VULKANGETTINGSTARTED_PIPELINES_H
