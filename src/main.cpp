#include <stdexcept>
#include <iostream>
#include <fstream>
#include <cstring>
#include <iomanip>

#include "jsonscene.h"
#include "app.h"
#include "crossplatform.h"

void help(const char* name)
{
    std::cout << "usage: " << name << " [-h] [SCENE [VALIDATION]]" << std::endl;
    std::cout << "    -h            print help message and exit" << std::endl;
    std::cout << "    SCENE         name of the scene to load, see scene folder, default is 'cube'" << std::endl;
    std::cout << "    VALIDATION    'y' or 'n' to enable validation, default is 'n'" << std::endl;
    std::cout << std::endl;
}

int main(int argc, char** argv)
{
    if(argc > 1 && (argv[1][0] == '-' && argv[1][1] == 'h'))
    {
        help(argv[0]);
        exit(0);
    }
    bool enableValidation = false;
    if(argc > 2 && argv[2][0] == 'y')
    {
        enableValidation = true;
    }
    const char* sceneName = "cube";
    if(argc > 1)
    {
        sceneName = argv[1];
    }

    json scene = loadScene(sceneName);

    if(enableValidation)
    {
        std::cout << std::setw(2) << scene << std::endl;
    }

    App app(1920, 1080, enableValidation, scene);

    try
    {
        app.init();
    }
    catch(const std::exception& e)
    {
        popup("Initialization error", e.what());
        return EXIT_FAILURE;
    }
    int frames = 0;
    float total = 0;

    try
    {
        while(app.shouldRun())
        {
            ++frames;
            glfwPollEvents();
            float delta = app.loop();

            total += delta;
            if(total >= 1)
            {
                std::cout << "FrameTime: " << std::setw(7) << total / frames << " - FPS " << std::setw(5) << frames << std::endl;
                frames = 0;
                total = 0;
            }
        }
    }
    catch(const std::exception& e)
    {
        popup("Runtime error", e.what());
    }

    try
    {
        app.shutdown();
    }
    catch(const std::exception& e)
    {
        popup("Shutdown error", e.what());
    }


    return 0;
}
