#ifndef VULKANGETTINGSTARTED_SWAPCHAIN_H
#define VULKANGETTINGSTARTED_SWAPCHAIN_H

#include <vector>

#include <vulkan/vulkan.h>

#include "instance.h"

struct Swapchain {
    VkSwapchainKHR handle = VK_NULL_HANDLE;
    std::vector<VkImage> images;
    std::vector<VkImageView> views;
    VkFormat format;
    VkExtent2D extent;

    void create(const Instance &instance, int width, int height, bool forceFifo);

    const VkSwapchainKHR& get() const;

    void clean(const Instance& instance);
};


#endif //VULKANGETTINGSTARTED_SWAPCHAIN_H
