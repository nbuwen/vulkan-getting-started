#ifndef TRIANGLE_ENUMS_H
#define TRIANGLE_ENUMS_H


#include <vulkan/vulkan.h>
#include <string>

std::string vulkanResult(VkResult result);
std::string physicalDeviceType(VkPhysicalDeviceType type);
std::string unpackVersion(uint32_t vulkanVersion);
std::string uuidToHex(const uint8_t* begin, size_t size);

#endif //TRIANGLE_ENUMS_H
