#include <iostream>

#include "lua.h"
#include "crossplatform.h"
#include "util.h"

GLFWwindow* Lua::window = nullptr;

Lua::Lua()
    : _state(luaL_newstate())
{
    luaL_openlibs(_state);
    registerGlmVec<glm::vec2>();
    registerGlmVec<glm::vec3>();
    registerGlmVec<glm::vec4>();

    registerKeys();
}

Lua::~Lua()
{
    lua_close(_state);
}

Lua::State Lua::state()
{
    return _state;
}

void Lua::pushi(int64_t value)
{
    lua_pushinteger(_state, value);
}

void Lua::pushd(double value)
{
    lua_pushnumber(_state, value);
}

void Lua::pushb(bool value)
{
    lua_pushboolean(_state, value);
}

void Lua::pushs(const char* value)
{
    lua_pushstring(_state, value);
}

template <typename T>
void Lua::pushv(T& value)
{
    using Trait = LuaVecTrait<T>;

    auto size = sizeof(T*);
    auto udata = lua_newuserdata(_state, size);
    *reinterpret_cast<T**>(udata) = &value;

    luaL_getmetatable(_state, Trait::Id);
    lua_setmetatable(_state, -2);
}

template void Lua::pushv<glm::vec2>(glm::vec2& value);
template void Lua::pushv<glm::vec3>(glm::vec3& value);
template void Lua::pushv<glm::vec4>(glm::vec4& value);

int64_t Lua::popi()
{
    auto value = luaL_checkinteger(_state, -1);
    pop(1);
    return value;
}

double Lua::popd()
{
    auto value = luaL_checknumber(_state, -1);
    pop(1);
    return value;
}

bool Lua::popb()
{
    auto value = luaL_checknumber(_state, -1);
    pop(1);
    return value != 0;
}

const char* Lua::pops()
{
    auto value = luaL_checkstring(_state, -1);
    pop(1);
    return value;
}

void Lua::pop(int number)
{
    lua_pop(_state, number);
}

void Lua::check(int error)
{
    if(error != LUA_OK)
    {
        const char* message = pops();
        std::cerr << "[Lua:Error] " << message << std::endl;
    }
}

void Lua::load(const std::string &script, const std::string &key)
{
    loadFile(script);

    if(lua_getglobal(_state, "Update") != LUA_TFUNCTION)
    {
        std::string error = "Failed to load script '" + script + "' with object key '" + key + "': global Update() function missing";
        throw std::runtime_error(error);
    }
    lua_setfield(_state, LUA_REGISTRYINDEX, key.c_str());

    //pop(1);
}

void Lua::update(const std::string& key, double totalTime, double deltaTime, Object& object, Camera& camera)
{
    lua_getfield(_state, LUA_REGISTRYINDEX, key.c_str());
    lua_newtable(_state);
    {
        pushd(totalTime);
        lua_setfield(_state, -2, "total");

        pushd(deltaTime);
        lua_setfield(_state, -2, "delta");
    }

    lua_newtable(_state);
    {
        pushv(object.position);
        lua_setfield(_state, -2, "pos");
        pushv(object.rotation);
        lua_setfield(_state, -2, "rot");
        pushv(object.scale);
        lua_setfield(_state, -2, "size");
        pushv(object.color);
        lua_setfield(_state, -2, "color");
    }

    lua_newtable(_state);
    {
        pushv(camera.position);
        lua_setfield(_state, -2, "pos");
    }
    check(lua_pcall(_state, 3, 0, 0));
}

template <typename T>
float * Lua::glmVecIndexPointer(State state)
{
    using Trait = LuaVecTrait<T>;

    auto v = *reinterpret_cast<T**>(luaL_checkudata(state, 1, Trait::Id));
    int64_t index = 0;
    if(lua_type(state, 2) == LUA_TSTRING)
    {
        const char* indexName = lua_tostring(state, 2);
        size_t length = strlen(indexName);

        if(length != 1)
        {
            return nullptr;
        }

        switch(indexName[0])
        {
        case 'x':
            index = 0;
            break;
        case 'y':
            index = 1;
            break;
        case 'z':
            index = 2;
            break;
        case 'w':
            index = 3;
            break;
        default:
            return nullptr;
        }

    }
    else
    {
        index = lua_tointeger(state, 2) - 1;
    }

    if(index < 0 || index >= Trait::Size)
    {
        return nullptr;
    }

    return Trait::IndexPointer(v, index);
}

template <typename T>
int Lua::glmVecIndex(Lua::State state)
{
    auto* item = glmVecIndexPointer<T>(state);
    if(item != nullptr)
    {
        lua_pushnumber(state, *item);
    }
    else
    {
        lua_rawget(state, 0);
    }

    return 1;
}

template <typename T>
int Lua::glmVecNewIndex(Lua::State state)
{
    auto* item = glmVecIndexPointer<T>(state);
    if(item != nullptr)
    {
        *item = static_cast<float>(luaL_checknumber(state, 3));
    }
    else
    {
        lua_rawset(state, 0);
    }

    return 0;
}

template <typename T>
int Lua::glmVecToString(Lua::State state)
{
    using Trait = LuaVecTrait<T>;

    auto v = *reinterpret_cast<T**>(luaL_checkudata(state, 1, Trait::Id));
    Trait::PushString(state, v);
    return 1;
}

template <typename T>
void Lua::registerGlmVec()
{
    using Trait = LuaVecTrait<T>;

    luaL_newmetatable(_state, Trait::Id);

    lua_pushcfunction(_state, Lua::glmVecToString<T>);
    lua_setfield(_state, -2, "__tostring");

    lua_pushcfunction(_state, Lua::glmVecIndex<T>);
    lua_setfield(_state, -2, "__index");

    lua_pushcfunction(_state, Lua::glmVecNewIndex<T>);
    lua_setfield(_state, -2, "__newindex");

    lua_pushcfunction(_state, Lua::glmVecLength<T>);
    lua_setfield(_state, -2, "length");

    lua_pushcfunction(_state, Lua::glmVecReset<T>);
    lua_setfield(_state, -2, "reset");

    lua_pushcfunction(_state, Lua::glmVecSet<T>);
    lua_setfield(_state, -2, "set");

    pop(1);
}

template<typename T>
int Lua::glmVecLength(Lua::State state)
{
    using Trait = LuaVecTrait<T>;

    auto v = *reinterpret_cast<T**>(luaL_checkudata(state, 1, Trait::Id));

    lua_pushnumber(state, glm::length(*v));

    return 1;
}

template<typename T>
int Lua::glmVecReset(Lua::State state)
{
    using Trait = LuaVecTrait<T>;

    auto v = *reinterpret_cast<T**>(luaL_checkudata(state, 1, Trait::Id));

    for(int i = 0; i < Trait::Size; ++i)
    {
        (*v)[i] = 0.0f;
    }

    return 0;
}

template<typename T>
int Lua::glmVecSet(Lua::State state)
{
    using Trait = LuaVecTrait<T>;

    auto v = *reinterpret_cast<T**>(luaL_checkudata(state, 1, Trait::Id));


    switch(lua_gettop(state))
    {
    case 2:  // self + other vector
        {
            auto w = *reinterpret_cast<T**>(luaL_checkudata(state, 2, Trait::Id));

            Trait::Set(v, w);
        }
        break;
    case Trait::Size + 1:  // self + x y z
        Trait::Set(state, v);
        break;
    default:
        luaL_error(state, "Failed to call set() with %d arguments. Either call set() with second vector or individual components)", lua_gettop(state) - 1);
        break;
    }

    return 0;
}


void Lua::loadFile(const std::string& script)
{
    std::string path = "scripts/";
    path = path + script + ".lua";
    if(luaL_loadfile(_state,path.c_str()))
    {
        path = "../" + path;
        if(luaL_loadfile(_state, path.c_str()))
        {
            std::string error = "Failed to load script '" + script + "': file does not exist";
            throw std::runtime_error(error);
        }
    }
    std::string abs = abspath(path);
    std::string dir = directory(abs);
    lua_pushstring(_state, abs.c_str());
    lua_pushstring(_state, dir.c_str());
    check(lua_pcall(_state, 2, 0, 0));
}

void Lua::loadFunction(const char* function)
{
    lua_getglobal(_state, function);
}

void Lua::call(int args, int returns)
{
    check(lua_pcall(_state, args, returns, 0));
}

void Lua::setWindow(GLFWwindow *window)
{
    Lua::window = window;
}

int Lua::windowKeyPressed(Lua::State state)
{
    auto key = (int)luaL_checkinteger(state, 1);

    lua_pushboolean(state, glfwGetKey(Lua::window, key) == GLFW_PRESS);

    return 1;
}

const char* KEY_NAMES[] = {
        "KEY_A", "KEY_B", "KEY_C", "KEY_D", "KEY_E", "KEY_F", "KEY_G", "KEY_H", "KEY_I", "KEY_J", "KEY_K", "KEY_L",
        "KEY_M", "KEY_N", "KEY_O", "KEY_P", "KEY_Q", "KEY_R", "KEY_S", "KEY_T", "KEY_U", "KEY_V", "KEY_W", "KEY_X",
        "KEY_Y", "KEY_Z",
        "KEY_0", "KEY_1", "KEY_2", "KEY_3", "KEY_4", "KEY_5", "KEY_6", "KEY_7", "KEY_8", "KEY_9",
};

void Lua::registerKeys()
{
    lua_pushcfunction(_state, windowKeyPressed);
    lua_setglobal(_state, "pressed");

    for(char key = GLFW_KEY_A; key <= GLFW_KEY_Z; ++key)
    {
        lua_pushinteger(_state, key);
        lua_setglobal(_state, KEY_NAMES[key - 'A']);
    }
    for(char key = GLFW_KEY_0; key <= GLFW_KEY_9; ++key)
    {
        lua_pushinteger(_state, key);
        lua_setglobal(_state, KEY_NAMES[26 + key - '0']);
    }
}
