#include "logging.h"


void logInstanceExtensions()
{
    unsigned int size = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &size, nullptr);
    std::vector<VkExtensionProperties> values(size);
    vkEnumerateInstanceExtensionProperties(nullptr, &size, values.data());

    std::clog << "Found " << values.size() << " instance extensions" << std::endl;
    for(auto ext : values)
    {
        std::clog << " - " << ext.extensionName << std::endl;
    }
}

void logInstanceLayers()
{
    unsigned int size = 0;
    vkEnumerateInstanceLayerProperties(&size, nullptr);
    std::vector<VkLayerProperties> values(size);
    vkEnumerateInstanceLayerProperties(&size, values.data());

    std::clog << "Found " << values.size() << " instance layers" << std::endl;
    for(auto layer : values)
    {
        std::clog << " - " << layer.layerName << std::endl;
    }
}