#include <glm/gtc/matrix_inverse.hpp>

#include "uniforms.h"

uint32_t PushConstants::totalSize(uint32_t lightCount)
{
    return vertexSize() + fragmentSize(lightCount);
}

uint32_t PushConstants::vertexOffset()
{
    return 0;
}

uint32_t PushConstants::vertexSize()
{
    return sizeof(totalTime);
}

uint32_t PushConstants::fragmentOffset()
{
    return 16;
}

uint32_t PushConstants::fragmentSize(uint32_t lightCount)
{
    return lightCount * sizeof(glm::vec3);
}

uint32_t PushConstants::fragmentSize() const
{
    return fragmentSize((uint32_t)lights.size());
}

void UniformBufferLocal::update()
{
    normal = glm::inverseTranspose(model);
}
