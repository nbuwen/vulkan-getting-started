#ifndef VULKANGETTINGSTARTED_ARRAYOBJECT_H
#define VULKANGETTINGSTARTED_ARRAYOBJECT_H

#include <vector>

#include <vulkan/vulkan.h>

#include "model.h"

struct Vertex;
struct Instance;

struct ArrayObject
{
    VkBuffer indexBuffer = VK_NULL_HANDLE;
    VkBuffer vertexBuffer = VK_NULL_HANDLE;
    VkDeviceMemory indexMemory = VK_NULL_HANDLE;
    VkDeviceMemory vertexMemory = VK_NULL_HANDLE;
    std::vector<Vertex> vertexData = {};
    std::vector<uint32_t> indexData = {};

    void create(const Instance& instance);
    void clean(const Instance &instance);
};


#endif //VULKANGETTINGSTARTED_ARRAYOBJECT_H
