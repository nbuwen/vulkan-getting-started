#include <stdexcept>
#include <cstring>

#define STB_IMAGE_IMPLEMENTATION
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
#include <stb_image.h>
#pragma GCC diagnostic pop

#include "texture.h"
#include "vkutil.h"
#include "uniforms.h"
#include "pipelines.h"


stbi_uc* loadImage(const std::string& filename, uint32_t* width= nullptr, uint32_t* height= nullptr, uint32_t* channels= nullptr)
{
    int w, h, c;
    std::string path = "textures/" + filename;
    stbi_uc* pixels = stbi_load(path.c_str(), &w, &h, &c, STBI_rgb_alpha);
    if(!pixels)
    {
        path = "../textures/" + filename;
        pixels = stbi_load(path.c_str(), &w, &h, &c, STBI_rgb_alpha);
    }
    if(!pixels)
    {
        std::string error = "Failed to open image " + filename;
        throw std::runtime_error(error);
    }
    if(width) *width = (uint32_t)w;
    if(height) *height = (uint32_t)h;
    if(channels) *channels = (uint32_t)c;

    return pixels;
}


void infoImage(const std::string& filename, uint32_t* width= nullptr, uint32_t* height= nullptr, uint32_t* channels= nullptr)
{
    int w, h, c;
    std::string path = "textures/" + filename;
    int success = stbi_info(path.c_str(), &w, &h, &c);
    if(!success)
    {
        path = "../textures/" + filename;
        success = stbi_info(path.c_str(), &w, &h, &c);
    }
    if(!success)
    {
        std::string error = "Failed to open image " + filename;
        throw std::runtime_error(error);
    }
    if(width) *width = (uint32_t)w;
    if(height) *height = (uint32_t)h;
    if(channels) *channels = (uint32_t)c;
}


void Texture::load(const Instance &instance, const std::string &filename)
{
    uint32_t width;
    uint32_t height;

    stbi_uc* pixels = loadImage(filename, &width, &height);

    VkDeviceSize size = (size_t)width * height * 4;

    VkBuffer staging;
    VkDeviceMemory stagingMemory;

    createBuffer(instance,
                 size,
                 VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                 staging,
                 stagingMemory);

    void* data;
    vkMapMemory(instance.device, stagingMemory, 0, size, 0, &data);
        memcpy(data, pixels, size);
    vkUnmapMemory(instance.device, stagingMemory);

    stbi_image_free(pixels);

    createImage(instance,
                width,
                height,
                0,
                VK_FORMAT_R8G8B8A8_UNORM,
                VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                image,
                memory);

    transitionImageLayout(instance,
                          image,
                          VK_FORMAT_R8G8B8A8_UNORM,
                          VK_IMAGE_LAYOUT_UNDEFINED,
                          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                          false);
    copyBufferToImage(instance, staging, image, width, height);
    transitionImageLayout(instance,
                          image,
                          VK_FORMAT_R8G8B8A8_UNORM,
                          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                          false);

    vkDestroyBuffer(instance.device, staging, nullptr);
    vkFreeMemory(instance.device, stagingMemory, nullptr);

    view = createImageView(instance, image, VK_IMAGE_VIEW_TYPE_2D, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
}

void Texture::loadSkybox(const Instance &instance, const nlohmann::json &box)
{
    const static std::array<const char*, 6> faceKeys = {"front", "back", "top", "bottom", "right", "left"};

    uint32_t width, height;
    infoImage(box["top"].get<std::string>(), &width, &height);

    for(uint32_t i = 1; i < faceKeys.size(); ++i)
    {
        uint32_t nextWidth, nextHeight;
        infoImage(box[faceKeys[i]].get<std::string>(), &nextWidth, &nextHeight);

        if(nextWidth != width || nextHeight != height)
        {
            throw std::runtime_error("Failed to create skybox images: dimensions don't match");
        }
    }

    VkDeviceSize size = width * height * 4;
    VkBuffer staging;
    VkDeviceMemory stagingMemory;

    createBuffer(instance,
                 size * 6,
                 VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT,
                 staging,
                 stagingMemory);

    void* data;
    uint32_t offset = 0;
    for(auto key : faceKeys)
    {
        vkMapMemory(instance.device, stagingMemory, offset, size, 0, &data);
            auto pixels = loadImage(box[key].get<std::string>());
            memcpy(data, pixels, size);
            stbi_image_free(pixels);
        vkUnmapMemory(instance.device, stagingMemory);
        offset += (uint32_t)size;
    }

    createImage(instance,
                width,
                height,
                VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT,
                VK_FORMAT_R8G8B8A8_UNORM,
                VK_IMAGE_TILING_OPTIMAL,
                VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
                image,
                memory);

    transitionImageLayout(instance,
                          image,
                          VK_FORMAT_R8G8B8A8_UNORM,
                          VK_IMAGE_LAYOUT_UNDEFINED,
                          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                          true);
    copyBufferToCube(instance, staging, image, width, height);
    transitionImageLayout(instance,
                          image,
                          VK_FORMAT_R8G8B8A8_UNORM,
                          VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                          VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                          true);

    vkDestroyBuffer(instance.device, staging, nullptr);
    vkFreeMemory(instance.device, stagingMemory, nullptr);

    view = createImageView(instance, image, VK_IMAGE_VIEW_TYPE_CUBE, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT);
}

void Texture::createDescriptorSet(const Instance &instance, const Pipelines &pipelines)
{
    VkDescriptorSetAllocateInfo allocate = {};
    allocate.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocate.descriptorPool = pipelines.descriptor.pool;
    allocate.descriptorSetCount = 1;
    allocate.pSetLayouts = &pipelines.descriptor.layout;

    if(hasError(vkAllocateDescriptorSets(instance.device, &allocate, &descriptorSet)))
    {
        throw std::runtime_error("Failed to allocate descriptor set");
    }

    VkDescriptorBufferInfo buffer = {};
    buffer.buffer = pipelines.uniform.global;
    buffer.offset = 0;
    buffer.range = sizeof(UniformBufferGlobal);

    VkDescriptorImageInfo descriptorImage = {};
	descriptorImage.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	descriptorImage.imageView = view;
	descriptorImage.sampler = pipelines.sampler;

    VkDescriptorBufferInfo dynamic = {};
    dynamic.buffer = pipelines.uniform.local;
    dynamic.offset = 0;
    dynamic.range = pipelines.uniform.localAlignment;

    std::array<VkWriteDescriptorSet, 3> writes = {};
    writes[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[0].dstSet = descriptorSet;
    writes[0].dstBinding = 0;
    writes[0].dstArrayElement = 0;
    writes[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    writes[0].descriptorCount = 1;
    writes[0].pBufferInfo = &buffer;

    writes[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[1].dstSet = descriptorSet;
    writes[1].dstBinding = 1;
    writes[1].dstArrayElement = 0;
    writes[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    writes[1].descriptorCount = 1;
    writes[1].pImageInfo = &descriptorImage;

    writes[2].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    writes[2].dstSet = descriptorSet;
    writes[2].dstBinding = 2;
    writes[2].dstArrayElement = 0;
    writes[2].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
    writes[2].descriptorCount = 1;
    writes[2].pBufferInfo = &dynamic;

    vkUpdateDescriptorSets(instance.device, (uint32_t)writes.size(), writes.data(), 0, nullptr);
}

void Texture::clean(const Instance &instance)
{
    vkDestroyImageView(instance.device, view, nullptr);
    vkDestroyImage(instance.device, image, nullptr);
    vkFreeMemory(instance.device, memory, nullptr);
}

