#ifndef VULKANGETTINGSTARTED_LUAWRAPPER_H
#define VULKANGETTINGSTARTED_LUAWRAPPER_H

#include <string>
#include <functional>

#include <lua.hpp>
#include <glm/glm.hpp>

#include "object.h"
#include "camera.h"

class Lua
{
public:
    using State = lua_State*;
    using Function = lua_CFunction;
    void check(int error);
private:
    static GLFWwindow* window;

    lua_State* _state;

    static int windowKeyPressed(State state);

    void registerKeys();

    template <typename T>
    static int glmVecToString(State state);
    template <typename T>
    static float * glmVecIndexPointer(State state);
    template <typename T>
    static int glmVecIndex(State state);
    template <typename T>
    static int glmVecNewIndex(State state);
    template <typename T>
    static int glmVecLength(State state);
    template <typename T>
    static int glmVecReset(State state);
    template <typename T>
    static int glmVecSet(State state);
    template <typename T>
    void registerGlmVec();
public:
    Lua();
    ~Lua();

    State state();

    void pushi(int64_t value);
    void pushd(double value);
    void pushb(bool value);
    void pushs(const char* value);
    template <typename T>
    void pushv(T& value);

    int64_t popi();
    double popd();
    bool popb();
    const char* pops();
    void pop(int number);

    void load(const std::string &script, const std::string &key);
    void update(const std::string& key, double totalTime, double deltaTime, Object& object, Camera& camera);

    void loadFile(const std::string&);
    void loadFunction(const char*);
    void call(int args, int returns);

    static void setWindow(GLFWwindow* window);
};

template <typename T>
struct LuaVecTrait
{
    using Type = T;
    static constexpr const int Size = 0;
    static constexpr const char* Id = nullptr;
    static void PushString(Lua::State, Type*) {};
    static float* IndexPointer(Type* v, int) {return nullptr;};
    static void Set(Type*, Type*) {};
    static void Set(Lua::State, Type*) {};
};

template<>
struct LuaVecTrait<glm::vec2>
{
    using Type = glm::vec2;
    static constexpr const int Size = 2;
    static constexpr const char* Id = "GlmVector2";
    static void PushString(Lua::State state, Type* v)
    {
        lua_pushfstring(state, "vec[%f, %f]", v->x, v->y);
    }
    static float* IndexPointer(Type* v, int index)
    {
        return &(*v)[index];
    }
    static void Set(Type* v, Type* w)
    {
        v->x = w->x;
        v->y = w->y;
    }
    static void Set(Lua::State state, Type* v)
    {
        v->x = (float)luaL_checknumber(state, 2);
        v->y = (float)luaL_checknumber(state, 3);
    };
};

template<>
struct LuaVecTrait<glm::vec3>
{
    using Type = glm::vec3;
    static constexpr const int Size = 3;
    static constexpr const char* Id = "GlmVector3";
    static void PushString(Lua::State state, Type* v)
    {
        lua_pushfstring(state, "vec[%f, %f, %f]", v->x, v->y, v->z);
    }
    static float* IndexPointer(Type* v, int index)
    {
        return &(*v)[index];
    }
    static void Set(Type* v, Type* w)
    {
        v->x = w->x;
        v->y = w->y;
        v->z = w->z;
    }
    static void Set(Lua::State state, Type* v)
    {
        v->x = (float)luaL_checknumber(state, 2);
        v->y = (float)luaL_checknumber(state, 3);
        v->z = (float)luaL_checknumber(state, 4);
    };
};

template<>
struct LuaVecTrait<glm::vec4>
{
    using Type = glm::vec4;
    static constexpr const int Size = 4;
    static constexpr const char* Id = "GlmVector4";
    static void PushString(Lua::State state, Type* v)
    {
        lua_pushfstring(state, "vec[%f, %f, %f, %f]", v->x, v->y, v->z, v->w);
    }
    static float* IndexPointer(Type* v, int index)
    {
        return &(*v)[index];
    }
    static void Set(Type* v, Type* w)
    {
        v->x = w->x;
        v->y = w->y;
        v->z = w->z;
        v->w = w->w;
    }
    static void Set(Lua::State state, Type* v)
    {
        v->x = (float)luaL_checknumber(state, 2);
        v->y = (float)luaL_checknumber(state, 3);
        v->z = (float)luaL_checknumber(state, 4);
        v->w = (float)luaL_checknumber(state, 5);
    };
};



#endif //VULKANGETTINGSTARTED_LUAWRAPPER_H
