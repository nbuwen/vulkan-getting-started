local _, PATH = ...
dofile(PATH.."solar.lua")

function Update(time, object, camera)
    object.pos:reset()
    Rotate(object.rot, -MOON_ROTATION, 0, -2.0)
    Orbit(object.pos, EARTH_DISTANCE, EARTH_SPEED)
    Orbit(object.pos, MOON_DISTANCE, MOON_SPEED)

    if pressed(KEY_M) then
        camera.pos:set(object.pos)
    end
end