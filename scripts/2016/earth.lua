local _, PATH = ...
dofile(PATH.."solar.lua")

local input = 0

function digit(d)
    input = input * 10 + d
    print("Input: ", input)
end

local was_pressed = {false, false, false, false, false, false, false, false, false, false}
local t_was_pressed = false

function Update(time, object, camera)
    Step(time.delta)
    object.pos:reset()
    Rotate(object.rot, EARTH_ROTATION, EARTH_TILT)
    Orbit(object.pos, EARTH_DISTANCE, EARTH_SPEED)

    if pressed(KEY_E) then
        camera.pos:set(object.pos)
    end

    for i = KEY_0, KEY_9 do
        if pressed(i) then
            if not was_pressed[i] then
                digit(i - KEY_0)
            end
            was_pressed[i] = true
        else
            was_pressed[i] = false
        end
    end

    if pressed(KEY_T) then
        if not t_was_pressed then
            print("Setting time scale to", input)
            Scale(input)
            input = 0
        end
        t_was_pressed = true
    else
        t_was_pressed = false
    end
end