PI = 3.1415926

function Rad(deg)
    return deg / 180.0 * PI
end

function Scale(s)
    TIME_SCALE = s
end

function Step(delta)
    TIME = TIME + delta * TIME_SCALE
end

SPEED_SCALE = 0.1
TIME_SCALE = 1

EARTH_DISTANCE = 10
MOON_DISTANCE = 2

EARTH_SPEED = SPEED_SCALE / 365.256 * PI
MOON_SPEED = SPEED_SCALE / 27.3217 * PI

EARTH_ROTATION = SPEED_SCALE * PI
MOON_ROTATION = MOON_SPEED

EARTH_TILT = Rad(23.439)

TIME = 0

function Orbit(position, distance, speed)
    position.x = position.x + math.cos(TIME * speed) * distance
    position.z = position.z + math.sin(TIME * speed) * distance
end

function Rotate(rotation, speed, tilt, phase)
    tilt = tilt or 0
    phase = phase or 0
    rotation.y = TIME * speed + phase
    rotation.x = tilt
end
