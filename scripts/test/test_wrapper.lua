print("Loading lua test script")

local function almost(x, y)
    return math.abs(x - y) < 0.001
end

local function almost2(vec, x, y)
    return almost(vec.x, x) and almost(vec.y, y)
end

local function almost3(vec, x, y, z)
    return almost2(vec, x, y) and almost(vec.z, z)
end

local function almost4(vec, x, y, z, w)
    return almost3(vec, x, y, z) and almost(vec.w, w)
end

local function TestVector2Unary(vec)
    -- __index
    assert(vec.x ~= nil, "vec2.x is nil")
    assert(vec.x == vec[1], "vec2.x != vec2[1]")
    assert(vec.y ~= nil, "vec2.y is nil")
    assert(vec.y == vec[2], "vec2.y != vec2[2]")

    -- __newindex
    vec.x = 100
    vec.y = 200
    assert(vec.x == 100, "cannot set vec2.x")
    assert(vec.y == 200, "cannot set vec2.y")
    vec[1] = 1000
    vec[2] = 2000
    assert(vec.x == 1000, "cannot set vec2[1]")
    assert(vec.y == 2000, "cannot set vec2[2]")

    -- __tostring
    vec.x = 1
    vec.y = 2
    assert(tostring(vec) == "vec[1.0, 2.0]", "vec2 to string is incorrect")
    vec.x = -2
    vec.y = -1
    assert(tostring(vec) == "vec[-2.0, -1.0]", "vec2 to string is incorrect")

    -- length()
    vec.x = 3
    vec.y = 4
    assert(vec:length() == 5, "vec2:length() is incorrect")
    vec.x = 1
    vec.y = -1
    assert(almost(vec:length(), math.sqrt(2)), "vec2:length() is incorrect")

    -- reset()
    vec:reset()
    assert(vec.x == 0 and vec.y == 0, "vec2:reset() is incorrect")

    -- set(x, y)
    vec:set(1, 2)
    assert(vec.x == 1 and vec.y == 2, "vec2:set() is incorrect")

    -- check in c++
    vec.x = -1000.2
    vec.y = -2000.3
end

local function TestVector3Unary(vec)
    assert(vec.x ~= nil, "vec3.x is nil")
    assert(vec.x == vec[1], "vec3.x != vec3[1]")
    assert(vec.y ~= nil, "vec3.y is nil")
    assert(vec.y == vec[2], "vec3.y != vec3[2]")
    assert(vec.z ~= nil, "vec3.z is nil")
    assert(vec.z == vec[3], "vec3.z != vec3[3]")

    -- __newindex
    vec.x = 100
    vec.y = 200
    vec.z = 300
    assert(vec.x == 100, "cannot set vec3.x")
    assert(vec.y == 200, "cannot set vec3.y")
    assert(vec.z == 300, "cannot set vec3.z")
    vec[1] = 1000
    vec[2] = 2000
    vec[3] = 3000
    assert(vec.x == 1000, "cannot set vec3[1]")
    assert(vec.y == 2000, "cannot set vec3[2]")
    assert(vec.z == 3000, "cannot set vec3[3]")

    -- __tostring
    vec.x = 1
    vec.y = 2
    vec.z = 3
    assert(tostring(vec) == "vec[1.0, 2.0, 3.0]", "vec3 to string is incorrect")
    vec.x = -3
    vec.y = -2
    vec.z = -1
    assert(tostring(vec) == "vec[-3.0, -2.0, -1.0]", "vec3 to string is incorrect")

    -- length()
    vec.x = 2
    vec.y = 3
    vec.z = 6
    assert(vec:length() == 7, "vec3:length() is incorrect")
    vec.x = 1
    vec.y = -1
    vec.z = 1
    assert(almost(vec:length(), math.sqrt(3)), "vec3:length() is incorrect")

    -- reset()
    vec:reset()
    assert(vec.x == 0 and vec.y == 0 and vec.z == 0, "vec3:reset() is incorrect")

    -- set(x, y, z)
    vec:set(1, 2, 3)
    assert(vec.x == 1 and vec.y == 2 and vec.z == 3, "vec3:set() is incorrect")

    -- check in c++
    vec.x = -100.2
    vec.y = -200.3
    vec.z = -300.4
end

local function TestVector4Unary(vec)
    -- __index
    assert(vec.x ~= nil, "vec4.x is nil")
    assert(vec.x == vec[1], "vec4.x != vec4[1]")
    assert(vec.y ~= nil, "vec4.y is nil")
    assert(vec.y == vec[2], "vec4.y != vec4[2]")
    assert(vec.z ~= nil, "vec4.z is nil")
    assert(vec.z == vec[3], "vec4.z != vec4[3]")
    assert(vec.w ~= nil, "vec4.w is nil")
    assert(vec.w == vec[4], "vec4.w != vec4[4]")

    -- __newindex
    vec.x = 100
    vec.y = 200
    vec.z = 300
    vec.w = 400
    assert(vec.x == 100, "cannot set vec4.x")
    assert(vec.y == 200, "cannot set vec4.y")
    assert(vec.z == 300, "cannot set vec4.z")
    assert(vec.w == 400, "cannot set vec4.w")
    vec[1] = 1000
    vec[2] = 2000
    vec[3] = 3000
    vec[4] = 4000
    assert(vec.x == 1000, "cannot set vec4[1]")
    assert(vec.y == 2000, "cannot set vec4[2]")
    assert(vec.z == 3000, "cannot set vec4[3]")
    assert(vec.w == 4000, "cannot set vec4[4]")

    -- __tostring
    vec.x = 1
    vec.y = 2
    vec.z = 3
    vec.w = 4
    assert(tostring(vec) == "vec[1.0, 2.0, 3.0, 4.0]", "vec4 to string is incorrect")
    vec.x = -4
    vec.y = -3
    vec.z = -2
    vec.w = -1
    assert(tostring(vec) == "vec[-4.0, -3.0, -2.0, -1.0]", "vec4 to string is incorrect")

    -- length()
    vec.x = 2
    vec.y = 3
    vec.z = 0
    vec.w = 6
    assert(vec:length() == 7, "vec4:length() is incorrect")
    vec.x = 1
    vec.y = -1
    vec.z = 1
    vec.w = 2
    assert(almost(vec:length(), math.sqrt(7)), "vec4:length() is incorrect")

    -- reset()
    vec:reset()
    assert(vec.x == 0 and vec.y == 0 and vec.z == 0 and vec.w == 0, "vec4:reset() is incorrect")

    -- set(x, y, z, w)
    vec:set(1, 2, 3, 4)
    assert(vec.x == 1 and vec.y == 2 and vec.z == 3 and vec.w == 4, "vec4:set() is incorrect")

    -- check in c++
    vec.x = -10.2
    vec.y = -20.3
    vec.z = -30.4
    vec.w = -40.5
end

function TestVector2Binary(source, target)
    -- set(vec)
    source.x = 42.1
    source.y = 1337.2
    target:set(source)

    assert(almost2(source, 42.1, 1337.2), "vec2:set() modified source")
    assert(almost2(target, 42.1, 1337.2), "vec2:set() failed to modify target")

    -- set(int*)
    local ok, _ = pcall(function() target:set(1) end)
    assert(not ok, "vec2:set(int) did not error")

    ok, _ = pcall(function() target:set(1, 2, 3) end)
    assert(not ok, "vec2:set(int, int) did not error")

    ok, _ = pcall(function() target:set(1, 2, 3, 4) end)
    assert(not ok, "vec2:set(int, int, int) did not error")
end

function TestVector3Binary(source, target)
    -- set(vec)
    source.x = 42.1
    source.y = 1337.2
    source.z = 9000.3
    target:set(source)

    assert(almost3(source, 42.1, 1337.2, 9000.3), "vec3:set() modified source")
    assert(almost3(target, 42.1, 1337.2, 9000.3), "vec3:set() failed to modify target")

    -- set(int*)
    local ok, _ = pcall(function() target:set(1) end)
    assert(not ok, "vec3:set(int) did not error")

    ok, _ = pcall(function() target:set(1, 2) end)
    assert(not ok, "vec3:set(int, int) did not error")

    ok, _ = pcall(function() target:set(1, 2, 3, 4) end)
    assert(not ok, "vec3:set(int, int, int, int) did not error")
end

function TestVector4Binary(source, target)
    -- set(vec)
    source.x = 42.1
    source.y = 1337.2
    source.z = 9000.3
    source.w = 0.4
    target:set(source)

    assert(almost4(source, 42.1, 1337.2, 9000.3, 0.4), "vec4:set() modified source")
    assert(almost4(target, 42.1, 1337.2, 9000.3, 0.4), "vec4:set() failed to modify target")

    -- set(int*)
    local ok, _ = pcall(function() target:set(1) end)
    assert(not ok, "vec4:set(int) did not error")

    ok, _ = pcall(function() target:set(1, 2) end)
    assert(not ok, "vec4:set(int, int) did not error")

    ok, _ = pcall(function() target:set(1, 2, 3) end)
    assert(not ok, "vec4:set(int, int, int) did not error")
end

function TestUnary(vec2, vec3, vec4)
    print("Testing Vector2 unary ...")
    TestVector2Unary(vec2)
    print("Done")
    print("")

    print("Testing Vector3 unary ...")
    TestVector3Unary(vec3)
    print("Done")
    print("")

    print("Testing Vector4 unary ...")
    TestVector4Unary(vec4)
    print("Done")
    print("")
end

function TestBinary(vec2a, vec2b, vec3a, vec3b, vec4a, vec4b)
    print("Testing Vector2 binary ...")
    TestVector2Binary(vec2a, vec2b)
    print("Done")
    print("")

    print("Testing Vector3 binary ...")
    TestVector3Binary(vec3a, vec3b)
    print("Done")
    print("")

    print("Testing Vector4 binary ...")
    TestVector4Binary(vec4a, vec4b)
    print("Done")
    print("")
end

print("Done")
print("")