
function Update(time, object, camera)
    local s = 0.3 *  (math.cos(time.total) + 2) / 2
    object.size.x = s
    object.size.y = s
    object.size.z = s
end