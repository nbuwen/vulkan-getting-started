
local function hsvToRgb(hue, saturation, value)

    if hue == 360.0 then
        hue = 0
    else
        hue = hue / 60
    end

    local fraction = hue - math.floor(hue)

    local p = value * (1.0 - saturation)
    local q = value * (1.0 - saturation * fraction)
    local t = value * (1.0 - saturation * (1.0 - fraction))

    if hue < 1.0 then
        return value, t, p
    elseif hue < 2.0 then
        return q, value, p
    elseif hue < 3.0 then
        return p, value, t
    elseif hue < 4.0 then
        return p, q, value
    elseif hue < 5.0 then
        return t, p, value
    else
        return value, p, q
    end
end

function Update(time, object, camera)

    h = (math.cos(time.total) + 1) * 180
    s = 1
    v = 1
    r, g, b = hsvToRgb(h, s, v)

    object.color.x = r
    object.color.y = g
    object.color.z = b
end