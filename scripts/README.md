Lua scripting
=============

In the scene json files each object *may* have a script file attached.

```json
  {
    "model": "...",
    "texture": "...",
    
    "script": "<name-of-lua-script-without-extension>"
  }
```

If a script file is provided via `"script"` it must be present in this `scripts` folder. Furthermore a global `Update`
function inside the script is required:

````lua
function Update(time, object, camera)
    -- Your code here
end 
````

The function will be called every frame. The following arguments will be passed:

- `time` *(table)* a table containing the following readonly attributes
  - `delta` *(number)* the delta time between the current and previous frame. Scaling changes to the objects with this factor yields a frame time independent animation
  - `total` *(total)* the total time elapsed since program start
- `object` *(table)* a table containing attributes describing the object this script is attached to
  - `pos` *(vec3)* the position of the current object. Changes to this variable will be reflected in the scene and translate the object.
  - `rot` *(vec3)* the rotation in euler angles of the current object. Changes to this variable will be reflected in the scene and rotate the object.
  - `size` *(vec3)* the scale on each axis of the current object. Changes to this variables will be reflected in the scene and scale the object.
  - `color` *(vec3)* the tint color of the current object. Changes to this variable will be reflected in the scene and change the tint color of the object.
- `camera` *(table)* a table containing attributes describing the camera in the scene
  - `pos` *(vec3)* the position of the camera. Changes to this variable will be reflected in the scene and translate the camera.

  Note that the names are arbitrary and only the position inside the argument list matters. Any values returned by `Update` are currently ignored.

The type `vec3` is a manually mapped `glm::vec3`. It's lua interface is as follows:

- `tostring(vec)` is implemented to show a string representation of the vector. E.g.: `"vec[1.0, 2.0, 3.0]"`.
- `vec.x, vec.y, vec.z` can be used to read and write the corresponding components of the vector.
- `vec[1], vec[2], vec[3]` can be used alternatively to the `x y z` fields
- `vec:length()` returns the length of the vector. I.e.: `math.sqrt(vec.x*vec.x + vec.y*vec.y + vec.z*vec.z)`
